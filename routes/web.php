<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontEndController@getFEHome')->name('getFEHome');
Route::get('/recent-release-anime', 'FrontEndController@getFERecentReleaseAnime')->name('getFERecentReleaseAnime');
Route::get('/ongoing-anime', 'FrontEndController@getFEOngoingAnime')->name('getFEOngoingAnime');
Route::get('/search-anime', 'FrontEndController@getFESearchAnime')->name('getFESearchAnime');
Route::get('/new-season-anime', 'FrontEndController@getFENewSeasonAnime')->name('getFENewSeasonAnime');
Route::get('/movies-anime', 'FrontEndController@getFEMoviesAnime')->name('getFEMoviesAnime');
Route::get('/genre/{slug}', 'FrontEndController@getFEGenreAnime')->name('getFEGenreAnime');
Route::get('/sub-category/{slug}', 'FrontEndController@getFESubCategoryAnime')->name('getFESubCategoryAnime');
Route::get('/anime/{slug}', 'FrontEndController@getFEAnime')->name('getFEAnime');
Route::get('/anime-list', 'FrontEndController@getFEAnimeList')->name('getFEAnimeList');
Route::get('/anime-list/{alphabet}', 'FrontEndController@getFEAnimeListByAlphabet')->name('getFEAnimeListByAlphabet');
Route::get('/{slug}', 'FrontEndController@getFEAnimeEpisode')->name('getFEAnimeEpisode');

Route::group(['prefix' => '/admin-access/'], function () {

	Route::get('/', 'AdminLoginController@getAdminLogin')->name('getAdminLogin'); 
	Route::get('/login', 'AdminLoginController@getAdminLogin')->name('getAdminLogin'); 
	Route::post('/login', 'AdminLoginController@postAdminLogin')->name('postAdminLogin'); 
	Route::get('/logout', 'AdminLoginController@getAdminLogout')->name('getAdminLogout')->middleware('custom_roles:superadmin,admin'); 
	Route::get('/dashboard', 'AdminDashboardController@getAdminDashboard')->name('getAdminDashboard')->middleware('custom_roles:superadmin,admin');

	Route::get('/change-password', 'AdminLoginController@getChangePassword')->name('getChangePassword')->middleware('custom_roles:superadmin,admin');
	Route::post('/change-password', 'AdminLoginController@postChangePassword')->name('postChangePassword')->middleware('custom_roles:superadmin,admin');

	Route::get('/reset-password', 'AdminLoginController@getResetPassword')->name('getResetPassword');
	Route::post('/reset-password', 'AdminLoginController@postResetPassword')->name('postResetPassword');
	
	// Source Host
	Route::get('/source-host', 'SourceHostController@getSourceHost')->name('getSourceHost')->middleware('custom_roles:superadmin,admin');
	Route::post('/source-host-ajax', 'SourceHostController@postAjaxGetSourceHost')->name('postAjaxGetSourceHost')->middleware('custom_roles:superadmin,admin');
	Route::get('/add-source-host', 'SourceHostController@getAddSourceHost')->name('getAddSourceHost')->middleware('custom_roles:superadmin,admin');
	Route::post('/add-source-host', 'SourceHostController@postAddSourceHost')->name('postAddSourceHost')->middleware('custom_roles:superadmin,admin');
	Route::get('/edit-source-host/{id}', 'SourceHostController@getEditSourceHost')->name('getEditSourceHost')->middleware('custom_roles:superadmin,admin');
	Route::post('/edit-source-host/{id}', 'SourceHostController@postEditSourceHost')->name('postEditSourceHost')->middleware('custom_roles:superadmin,admin');
	Route::post('/delete-source-host/{id}', 'SourceHostController@postDeleteSourceHost')->name('postDeleteSourceHost')->middleware('custom_roles:superadmin,admin');

	// Get Recent Release Anime
	Route::post('/recent-release-anime', 'CrawlerController@getRecentReleaseAnime')->name('getRecentReleaseAnime')->middleware('custom_roles:superadmin,admin');

	// Get Detail Anime
	Route::post('/detail-anime', 'CrawlerController@getAnime')->name('getAnime')->middleware('custom_roles:superadmin,admin');

	// Get Movies Anime
	Route::post('/movies-anime', 'CrawlerController@getMoviesAnime')->name('getMoviesAnime')->middleware('custom_roles:superadmin,admin');

	// Anime
	Route::get('/anime', 'AnimeController@getAnime')->name('getAnime')->middleware('custom_roles:superadmin,admin');
	Route::post('/anime-ajax', 'AnimeController@postAjaxAnime')->name('postAjaxAnime')->middleware('custom_roles:superadmin,admin');
	Route::get('/add-anime', 'AnimeController@getAddAnime')->name('getAddAnime')->middleware('custom_roles:superadmin,admin');
	Route::post('/add-anime', 'AnimeController@postAddAnime')->name('postAddAnime')->middleware('custom_roles:superadmin,admin');
	Route::get('/edit-anime/{id}', 'AnimeController@getEditAnime')->name('getEditAnime')->middleware('custom_roles:superadmin,admin');
	Route::post('/edit-anime/{id}', 'AnimeController@postEditAnime')->name('postEditAnime')->middleware('superadmin');
	Route::post('/delete-anime/{id}', 'AnimeController@postDeleteAnime')->name('postDeleteAnime')->middleware('superadmin');

	// Anime Genre
	Route::get('/anime-genre-with-anime-id/{anime_id}', 'AnimeGenreController@getAnimeGenre')->name('getAnimeGenre')->middleware('custom_roles:superadmin,admin');
	Route::post('/anime-genre-ajax-with-anime-id/{anime_id}', 'AnimeGenreController@postAjaxAnimeGenre')->name('postAjaxAnimeGenre')->middleware('custom_roles:superadmin,admin');
	Route::get('/add-anime-genre-with-anime-id/{anime_id}', 'AnimeGenreController@getAddAnimeGenre')->name('getAddAnimeGenre')->middleware('custom_roles:superadmin,admin');
	Route::post('/add-anime-genre-with-anime-id/{anime_id}', 'AnimeGenreController@postAddAnimeGenre')->name('postAddAnimeGenre')->middleware('custom_roles:superadmin,admin');
	Route::get('/edit-anime-genre/{id}/with-anime-id/{anime_id}', 'AnimeGenreController@getEditAnimeGenre')->name('getEditAnimeGenre')->middleware('custom_roles:superadmin,admin');
	Route::post('/edit-anime-genre/{id}/with-anime-id/{anime_id}', 'AnimeGenreController@postEditAnimeGenre')->name('postEditAnimeGenre')->middleware('custom_roles:superadmin');
	Route::post('/delete-anime-genre/{id}/with-anime-id/{anime_id}', 'AnimeGenreController@postDeleteAnimeGenre')->name('postDeleteAnimeGenre')->middleware('custom_roles:superadmin');

	// Anime Episode
	Route::get('/anime-episode-with-anime-id/{anime_id}', 'AnimeEpisodeController@getAnimeEpisode')->name('getAnimeEpisode')->middleware('custom_roles:superadmin,admin');
	Route::post('/anime-episode-ajax-with-anime-id/{anime_id}', 'AnimeEpisodeController@postAjaxAnimeEpisode')->name('postAjaxAnimeEpisode')->middleware('custom_roles:superadmin,admin');
	Route::get('/add-anime-episode-with-anime-id/{anime_id}', 'AnimeEpisodeController@getAddAnimeEpisode')->name('getAddAnimeEpisode')->middleware('custom_roles:superadmin,admin');
	Route::post('/add-anime-episode-with-anime-id/{anime_id}', 'AnimeEpisodeController@postAddAnimeEpisode')->name('postAddAnimeEpisode')->middleware('custom_roles:superadmin,admin');
	Route::get('/edit-anime-episode/{id}/with-anime-id/{anime_id}', 'AnimeEpisodeController@getEditAnimeEpisode')->name('getEditAnimeEpisode')->middleware('custom_roles:superadmin,admin');
	Route::post('/edit-anime-episode/{id}/with-anime-id/{anime_id}', 'AnimeEpisodeController@postEditAnimeEpisode')->name('postEditAnimeEpisode')->middleware('custom_roles:superadmin');
	Route::post('/delete-anime-episode/{id}/with-anime-id/{anime_id}', 'AnimeEpisodeController@postDeleteAnimeEpisode')->name('postDeleteAnimeEpisode')->middleware('custom_roles:superadmin');

	// Anime Streaming
	Route::get('/anime-streaming-with-anime-episode-id/{anime_episode_id}', 'AnimeStreamingController@getAnimeStreaming')->name('getAnimeStreaming')->middleware('custom_roles:superadmin,admin');
	Route::post('/anime-streaming-ajax-with-anime-episode-id/{anime_episode_id}', 'AnimeStreamingController@postAjaxAnimeStreaming')->name('postAjaxAnimeStreaming')->middleware('custom_roles:superadmin,admin');
	Route::get('/add-anime-streaming-with-anime-episode-id/{anime_episode_id}', 'AnimeStreamingController@getAddAnimeStreaming')->name('getAddAnimeStreaming')->middleware('custom_roles:superadmin,admin');
	Route::post('/add-anime-streaming-with-anime-episode-id/{anime_episode_id}', 'AnimeStreamingController@postAddAnimeStreaming')->name('postAddAnimeStreaming')->middleware('custom_roles:superadmin,admin');
	Route::get('/edit-anime-streaming/{id}/with-anime-episode-id/{anime_episode_id}', 'AnimeStreamingController@getEditAnimeStreaming')->name('getEditAnimeStreaming')->middleware('custom_roles:superadmin,admin');
	Route::post('/edit-anime-streaming/{id}/with-anime-episode-id/{anime_episode_id}', 'AnimeStreamingController@postEditAnimeStreaming')->name('postEditAnimeStreaming')->middleware('custom_roles:superadmin');
	Route::post('/delete-anime-streaming/{id}/with-anime-episode-id/{anime_episode_id}', 'AnimeStreamingController@postDeleteAnimeStreaming')->name('postDeleteAnimeStreaming')->middleware('custom_roles:superadmin');

	// Anime Download
	Route::get('/anime-download-with-anime-episode-id/{anime_episode_id}', 'AnimeDownloadController@getAnimeDownload')->name('getAnimeDownload')->middleware('custom_roles:superadmin,admin');

	Route::post('/anime-download-ajax-with-anime-episode-id/{anime_episode_id}', 'AnimeDownloadController@postAjaxAnimeDownload')->name('postAjaxAnimeDownload')->middleware('custom_roles:superadmin,admin');

	Route::get('/add-anime-download-with-anime-episode-id/{anime_episode_id}', 'AnimeDownloadController@getAddAnimeDownload')->name('getAddAnimeDownload')->middleware('custom_roles:superadmin,admin');

	Route::post('/add-anime-download-with-anime-episode-id/{anime_episode_id}', 'AnimeDownloadController@postAddAnimeDownload')->name('postAddAnimeDownload')->middleware('custom_roles:superadmin,admin');

	Route::get('/edit-anime-download/{id}/with-anime-episode-id/{anime_episode_id}', 'AnimeDownloadController@getEditAnimeDownload')->name('getEditAnimeDownload')->middleware('custom_roles:superadmin,admin');

	Route::post('/edit-anime-download/{id}/with-anime-episode-id/{anime_episode_id}', 'AnimeDownloadController@postEditAnimeDownload')->name('postEditAnimeDownload')->middleware('custom_roles:superadmin');

	Route::post('/delete-anime-download/{id}/with-anime-episode-id/{anime_episode_id}', 'AnimeDownloadController@postDeleteAnimeDownload')->name('postDeleteAnimeDownload')->middleware('custom_roles:superadmin');

	// Data Customer
	Route::get('/customer', 'UserController@getCustomer')->name('getCustomer')->middleware('custom_roles:superadmin,admin');
	Route::post('/customer-ajax', 'UserController@postAjaxCustomer')->name('postAjaxCustomer')->middleware('custom_roles:superadmin,admin');
	Route::get('/add-customer', 'UserController@getAddCustomer')->name('getAddCustomer')->middleware('custom_roles:superadmin');
	Route::post('/add-customer', 'UserController@postAddCustomer')->name('postAddCustomer')->middleware('custom_roles:superadmin');
	Route::get('/edit-customer/{id}', 'UserController@getEditCustomer')->name('getEditCustomer')->middleware('custom_roles:superadmin');
	Route::post('/edit-customer/{id}', 'UserController@postEditCustomer')->name('postEditCustomer')->middleware('superadmin');
	Route::post('/delete-customer/{id}', 'UserController@postDeleteCustomer')->name('postDeleteCustomer')->middleware('superadmin');

	// Data Admin
	Route::get('/admin', 'UserController@getAdmin')->name('getAdmin')->middleware('custom_roles:superadmin,admin');
	Route::post('/admin-ajax', 'UserController@postAjaxAdmin')->name('postAjaxAdmin')->middleware('custom_roles:superadmin,admin');
	Route::get('/add-admin', 'UserController@getAddAdmin')->name('getAddAdmin')->middleware('custom_roles:superadmin');
	Route::post('/add-admin', 'UserController@postAddAdmin')->name('postAddAdmin')->middleware('custom_roles:superadmin');
	Route::get('/edit-admin/{id}', 'UserController@getEditAdmin')->name('getEditAdmin')->middleware('custom_roles:superadmin');
	Route::post('/edit-admin/{id}', 'UserController@postEditAdmin')->name('postEditAdmin')->middleware('superadmin');
	Route::post('/delete-admin/{id}', 'UserController@postDeleteAdmin')->name('postDeleteAdmin')->middleware('superadmin');

	// Data Customer
	Route::get('/customer', 'UserController@getCustomer')->name('getCustomer')->middleware('custom_roles:superadmin,admin');
	Route::post('/customer-ajax', 'UserController@postAjaxCustomer')->name('postAjaxCustomer')->middleware('custom_roles:superadmin,admin');
	Route::get('/add-customer', 'UserController@getAddCustomer')->name('getAddCustomer')->middleware('custom_roles:superadmin');
	Route::post('/add-customer', 'UserController@postAddCustomer')->name('postAddCustomer')->middleware('custom_roles:superadmin');
	Route::get('/edit-customer/{id}', 'UserController@getEditCustomer')->name('getEditCustomer')->middleware('custom_roles:superadmin');
	Route::post('/edit-customer/{id}', 'UserController@postEditCustomer')->name('postEditCustomer')->middleware('superadmin');
	Route::post('/delete-customer/{id}', 'UserController@postDeleteCustomer')->name('postDeleteCustomer')->middleware('superadmin');

	// Data All User
	Route::get('/user', 'UserController@getUser')->name('getUser')->middleware('custom_roles:superadmin');
	Route::post('/user-ajax', 'UserController@postAjaxUser')->name('postAjaxUser')->middleware('custom_roles:superadmin');
	Route::get('/add-user', 'UserController@getAddUser')->name('getAddUser')->middleware('custom_roles:superadmin');
	Route::post('/add-user', 'UserController@postAddUser')->name('postAddUser')->middleware('custom_roles:superadmin');
	Route::get('/edit-user/{id}', 'UserController@getEditUser')->name('getEditUser')->middleware('custom_roles:superadmin');
	Route::post('/edit-user/{id}', 'UserController@postEditUser')->name('postEditUser')->middleware('superadmin');
	Route::post('/delete-user/{id}', 'UserController@postDeleteUser')->name('postDeleteUser')->middleware('superadmin');

});
