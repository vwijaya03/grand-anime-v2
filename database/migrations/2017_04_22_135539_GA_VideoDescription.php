<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GAVideoDescription extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('GA_Video', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->text('sub_category');
            $table->text('sub_category_slug');
            $table->string('img');
            $table->string('slug');
            $table->string('title');
            $table->longText('description');
            $table->string('released');
            $table->string('status');
            $table->timestamps();
            $table->integer('delete');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('GA_VideoDescription');
    }
}
