<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GAConnectionVideoDescAndGenre extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('GA_ConnectionVideoDescAndGenre', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->integer('video_description_id');
            $table->string('genre_slug');
            $table->string('genre_text');
            $table->timestamps();
            $table->string('delete');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('GA_ConnectionVideoDescAndGenre');
    }
}
