<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GAReportVideo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('GA_ReportVideo', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('video_episode_id');
            $table->string('url_video');
            $table->string('problem');
            $table->string('other_problem');
            $table->timestamps();
            $table->integer('delete');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('GA_ReportVideo');
    }
}
