<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SourceHost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('GA_SourceHost', function (Blueprint $table) {
            $table->increments('id');
            $table->string('protocol');
            $table->string('url');
            $table->string('additional_url')->nullable();
            $table->string('recent_page')->nullable();
            $table->string('episode_start_page')->nullable();
            $table->string('episode_end_page')->nullable();
            $table->string('tipe');
            $table->timestamps();
            $table->integer('delete');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('GA_SourceHost');
    }
}
