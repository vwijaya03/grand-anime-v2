<?php

use Illuminate\Database\Seeder;
use App\UserModel;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserModel::create
    		([
    			'name' => "Viko Wijaya",
    			'email' => "viko_wijaya@yahoo.co.id",
    			'password' => Hash::make("viko"),
    			'role' => "superadmin",
    			'delete' => 0
    		]);

        UserModel::create
        ([
            'name' => "Mamun Widodo",
            'email' => "mamun@mail.com",
            'password' => Hash::make("mamun"),
            'role' => "admin",
            'delete' => 0
        ]);

        UserModel::create
        ([
            'name' => "Naomi Sakura",
            'email' => "ns@mail.com",
            'password' => Hash::make("naomi"),
            'role' => "customer",
            'delete' => 0
        ]);
    }
}
