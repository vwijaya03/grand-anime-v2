<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Superadmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()) {
            if(auth()->user()->role == 'superadmin') {
                return $next($request);
            } else {
                Auth::logout();
                return redirect()->route('getAdminLogin')->with(['err' => 'Unauthorized !']);
            }
        } else {
            Auth::logout();
            return redirect()->route('getAdminLogin')->with(['err' => 'Please login again !']);
        }
    }
}
