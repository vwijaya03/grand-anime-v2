<?php

namespace App\Http\Middleware;

use Closure, Auth;

class Customer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$args)
    {
        if(Auth::check()) {
            if(auth()->user()->role == 'customer') {
                return $next($request);
            } else {
                Auth::logout();
                return redirect()->route('getAdminLogin')->with(['err' => 'Unauthorized !']);
            }
        } else {
            Auth::logout();
            return redirect()->route('getAdminLogin')->with(['err' => 'Please login again !']);
        }
    }
}
