<?php

namespace App\Http\Middleware;

use Closure, Auth;

class CustomRoles
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$args)
    {
        if(Auth::check()) {

            $user_role = [];
            $user_role[] = auth()->user()->role;

            $authorizedRoles = array_intersect($args, $user_role);

            if(count($authorizedRoles) > 0 && auth()->user()->delete == 0) {
                return $next($request);
            } else {
                Auth::logout();
                return redirect()->route('getAdminLogin')->with(['err' => 'Unauthorized !']);
            }
        } else {
            Auth::logout();
            return redirect()->route('getAdminLogin')->with(['err' => 'Please login again !']);
        }
    }
}
