<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth, Hash, DB, Log, Carbon;
use RuntimeException;
use Goutte\Client;
use App\KategoriModel;
use App\VideoDescriptionModel;
use App\VideoEpisodeModel;
use App\ConnectVideoAndGenreModel;
use App\StreamingUrlModel;
use App\DownloadUrlModel;

class CrawlerController extends Controller
{
    public function __construct()
    {
        DB::enableQueryLog();
    }

    public function getMoviesAnime()
    {
    	$requested = request()->validate([
    		'target_url' => 'required',
    		'base_url' => 'required',
    		'ep_start' => 'required',
    		'ep_end' => 'required'
    	]);

    	// $target_url = 'https://ww4.gogoanime.io/anime-movies.html?page=2';
    	$target_url = $requested['target_url'];
    	// $base_url = 'https://ww4.gogoanime.io';
    	$base_url = $requested['base_url'];

    	$plot_summary = "Plot Summary: ";
    	$plot_summary_len = strlen($plot_summary);

    	$released = "Released: ";
    	$released_len = strlen($released);

    	$ongoing_status = "Status: ";
    	$ongoing_status_len = strlen($ongoing_status);

    	$remove_category_string_for_slug = strlen("/category/");
    	$remove_sub_category_string_for_slug = strlen("/sub-category/");
    	$remove_choose_this_server_string = strlen("Choose this server");
    	$minus_remove_choose_this_server_string = $remove_choose_this_server_string * -1;

    	$result_anime_detail = [];
    	$result_video_episode = [];
		$result_video_streaming = [];
		$result_video_download = [];
    	$result_all_movies = [];

    	$movies_client = new Client();

    	$crawler_movies = $movies_client->request('GET', $target_url);

		$data_movies = $crawler_movies->filter('.items .name a')->each(function ($node_anime, $i) use($crawler_movies, $movies_client, $remove_sub_category_string_for_slug, $remove_category_string_for_slug, $minus_remove_choose_this_server_string, $base_url, $plot_summary_len, $released_len, $ongoing_status_len, $result_anime_detail, $result_video_episode, $result_video_streaming, $result_video_download, $result_all_movies, $requested) {
	    	
	    	Log::info('Anime: '.$node_anime->text());

    		$anime_detail_link = $crawler_movies->selectLink(trim($node_anime->text()))->link();
			$crawler_anime_detail = $movies_client->click($anime_detail_link);

	    	$anime_slug = substr($crawler_anime_detail->filter('head link[rel=canonical]')->attr('href'), $remove_category_string_for_slug);
	    	$result_anime_detail['anime_slug'] = $anime_slug;
	    	Log::info($anime_slug);

			$anime_image = $crawler_anime_detail->filter('.anime_info_body_bg img')->attr('src');
			$result_anime_detail['anime_image'] = $anime_image;
			Log::info($anime_image);

			$anime_title = $crawler_anime_detail->filter('.anime_info_body_bg h1')->each(function ($node) {
			    return $node->text();
			});
			$result_anime_detail['anime_title'] = $anime_title[0];
			Log::info($anime_title[0]);

			$anime_sub_category = $crawler_anime_detail->filter('.anime_info_body_bg a')->eq(1)->each(function ($node) {
			    return $node->text();
			});
			$result_anime_detail['anime_sub_category'] = $anime_sub_category[0];
			Log::info($anime_sub_category[0]);

			$anime_sub_category_slug = substr($crawler_anime_detail->filter('.anime_info_body_bg a')->eq(1)->attr('href'), $remove_sub_category_string_for_slug);
			$result_anime_detail['anime_sub_category_slug'] = $anime_sub_category_slug;
			Log::info($anime_sub_category_slug);

			$anime_description = $crawler_anime_detail->filter('.anime_info_body_bg p')->eq(2)->each(function ($node) use ($plot_summary_len) {
				return substr($node->text(), $plot_summary_len);
			});
			$result_anime_detail['anime_description'] = $anime_description[0];
			Log::info($anime_description[0]);

			$anime_released = $crawler_anime_detail->filter('.anime_info_body_bg p')->eq(4)->each(function ($node) use ($released_len) {
				return substr($node->text(), $released_len);
			});
			$result_anime_detail['anime_released'] = $anime_released[0];
			Log::info($anime_released[0]);

			$anime_status = $crawler_anime_detail->filter('.anime_info_body_bg p')->eq(5)->each(function ($node) use ($ongoing_status_len) {
				return substr($node->text(), $ongoing_status_len);
			});
			$result_anime_detail['anime_status'] = $anime_status[0];
			Log::info($anime_status[0]);

			$anime_genre_texts = $crawler_anime_detail->filter('.anime_info_body_bg a')->extract(array('title'));

			unset($anime_genre_texts[0]);
			unset($anime_genre_texts[1]);

			$result_anime_detail['anime_genre'] = $anime_genre_texts;

			$ep_start = $requested['ep_start'];
			// $ep_start = 1;
			$ep_end = $requested['ep_end'];
			// $ep_end = 900;
			$movie_id = $crawler_anime_detail->filter('input#movie_id')->attr('value');
			Log::info('Movie ID: '.$movie_id);

			$crawler_anime_detail->clear();
			unset($crawler_anime_detail);

			// $get_load_list_episode_url = $requested['base_url'].'/load-list-episode?ep_start='.$ep_start.'&ep_end='.$ep_end.'&id='.$movie_id;
			$get_load_list_episode_url = 'https://ww4.gogoanime.io'.'/load-list-episode?ep_start='.$ep_start.'&ep_end='.$ep_end.'&id='.$movie_id;
			// Log::info($get_load_list_episode_url);
			$result_all_video_episode = [];

			$get_episode_lists = new Client();

	    	$crawler_episode_lists = $get_episode_lists->request('GET', $get_load_list_episode_url);

	    	$data_video_episodes = $crawler_episode_lists->filter('li .name')->each(function ($node_episode, $i) use ($crawler_episode_lists, $get_episode_lists, $remove_sub_category_string_for_slug, $remove_category_string_for_slug, $minus_remove_choose_this_server_string, $result_video_episode, $result_video_streaming, $result_video_download, $result_all_movies, $result_all_video_episode) {

	    		Log::info('Anime Episode: '.$node_episode->text());
				$episode_links = $crawler_episode_lists->selectLink(trim($node_episode->text()))->link();
				$crawler_episode_links = $get_episode_lists->click($episode_links);

				// Title
				$anime_title = $crawler_episode_links->filter('.title_name h2')->each(function ($node_anime_title) {
					return $node_anime_title->text();
				});
				$result_video_episode['anime_title'] = $anime_title[0];

				// Sub Category
				$sub_category = $crawler_episode_links->filter('.anime_video_body_cate a')->attr('title');
				$result_video_episode['sub_category'] = $sub_category;

				// Sub Category Slug
				$sub_category_slug = substr($crawler_episode_links->filter('.anime_video_body_cate a')->attr('href'), $remove_sub_category_string_for_slug);
				$result_video_episode['sub_category_slug'] = $sub_category_slug;


				// Anime Detail Slug
				$anime_detail_slug = substr($crawler_episode_links->filter('head link[rel=canonical]')->attr('href'), 1);
				$result_video_episode['anime_detail_slug'] = $anime_detail_slug;

				// Anime Info
				$anime_info = $crawler_episode_links->filter('.anime-info a')->attr('title');
				// print_r($anime_info);
				$result_video_episode['anime_info'] = $anime_info;

				// Anime Info Slug
				$anime_info_slug = substr($crawler_episode_links->filter('.anime-info a')->attr('href'), $remove_category_string_for_slug);
				$result_video_episode['anime_info_slug'] = $anime_info_slug;

				$streaming_links = $crawler_episode_links->filter('a[data-video]')->extract(array('data-video'));

				for ($i=0; $i < count($streaming_links); $i++) { 

				    $findme   = 'https:';
				    $pos = strpos($streaming_links[$i], $findme);

				    // Note our use of ===. Simply, == would not work as expected
				    // because the position of 'a' was the 0th (first) character.
				    if ($pos === false) {
				        $streaming_links[$i] = 'https:'.$streaming_links[$i];
				    }
				}

				// Streaming Link Names
				$streaming_link_names = $crawler_episode_links->filter('a[data-video]')->each(function ($node_streaming_link_names) use ($minus_remove_choose_this_server_string) {
				    return substr($node_streaming_link_names->text(), 0, $minus_remove_choose_this_server_string);
				});

				$result_video_streaming[0] = $streaming_links;
				$result_video_streaming[1] = $streaming_link_names;
				
				$link = $crawler_episode_links->selectLink('Download')->link();
				$crawler_download = $get_episode_lists->click($link);

				// Download Links
				$download_links = $crawler_download->filter('.dowload a')->extract(array('href'));

				// Download Link Names
				$download_text_names = $crawler_download->filter('.dowload a')->each(function ($node_download_text_names) {
				    return $node_download_text_names->text();
				});

				$result_video_download[0] = $download_links;
				$result_video_download[1] = $download_text_names;

				$result_all_video_episode['anime_detail_episode'] = $result_video_episode;
				$result_all_video_episode['anime_streaming'] = $result_video_streaming;
				$result_all_video_episode['anime_download'] = $result_video_download;

				$crawler_download->clear();
				unset($crawler_download);

				return $result_all_video_episode;
			});

			$crawler_episode_lists->clear();
			unset($crawler_episode_lists);
			unset($get_episode_lists);

			$result_all_movies['anime_detail'] = $result_anime_detail;
			$result_all_movies['anime_episodes'] = $data_video_episodes;

			return $result_all_movies;
		});

		$crawler_movies->clear();
		unset($crawler_movies);
		unset($movies_client);
		
		for ($i=0; $i < count($data_movies); $i++) { 
			
			$video_id = '';

			$is_exist_video = VideoDescriptionModel::select('id')
			->where('slug', $data_movies[$i]['anime_detail']['anime_slug'])
			->where('delete', 0)
			->first();

			if($is_exist_video != null){
				$video_id = $is_exist_video->id;
				Log::info('ada yang sudah ada / tidak bisa mendeteksi node text nya untuk di click');
				Log::info($data_movies[$i]['anime_detail']['anime_slug']);
			} else {
				$result_detail_anime = $get_video = VideoDescriptionModel::create([
					'user_id' => 1,
					'sub_category' => $data_movies[$i]['anime_detail']['anime_sub_category'],
					'sub_category_slug' => $data_movies[$i]['anime_detail']['anime_sub_category_slug'],
					'img' => $data_movies[$i]['anime_detail']['anime_image'],
					'slug' => $data_movies[$i]['anime_detail']['anime_slug'],
					'title' => $data_movies[$i]['anime_detail']['anime_title'],
					'description' => $data_movies[$i]['anime_detail']['anime_description'],
					'released' => $data_movies[$i]['anime_detail']['anime_released'],
					'status' => $data_movies[$i]['anime_detail']['anime_status'],
					'delete' => 0 
				]);

				$video_id = $result_detail_anime->id;
			}

			foreach ($data_movies[$i]['anime_detail']['anime_genre'] as $anime_genre) {

				$is_exist_genre = ConnectVideoAndGenreModel::select('id')
				->where('video_description_id', $video_id)
				->where('genre_slug', str_slug($anime_genre, '-'))
				->where('delete', 0)
				->first();

				if($is_exist_genre == null)
				{
					ConnectVideoAndGenreModel::create([
						'user_id' => 1,
						'video_description_id' => $video_id,
						'genre_slug' => str_slug($anime_genre, '-'),
						'genre_text' => $anime_genre,
						'delete' => 0
					]);
				}
			}

			$video_episode_id = '';

			for ($j=0; $j < count($data_movies[$i]['anime_episodes']); $j++) { 

				$is_exist_video_episode = VideoEpisodeModel::select('GA_VideoEpisode.id')
				->where('video_description_id', $video_id)
				->where('slug', $data_movies[$i]['anime_episodes'][$j]['anime_detail_episode']['anime_detail_slug'])
				->where('delete', 0)
				->first();

				if($is_exist_video_episode != null) {
					$video_episode_id = $is_exist_video_episode->id;
				} else {
					$result_anime_episode_detail = VideoEpisodeModel::create([
						'video_description_id' => $video_id,
						'slug' => $data_movies[$i]['anime_episodes'][$j]['anime_detail_episode']['anime_detail_slug'],
						'title' => $data_movies[$i]['anime_episodes'][$j]['anime_detail_episode']['anime_title'],
						'delete' => 0
					]);

					$video_episode_id = $result_anime_episode_detail->id;
				}

				if(count($data_movies[$i]['anime_episodes'][$j]['anime_streaming'][0]) == count($data_movies[$i]['anime_episodes'][$j]['anime_streaming'][1])) {

					for ($k=0; $k < count($data_movies[$i]['anime_episodes'][$j]['anime_streaming'][0]); $k++) { 

						$is_exist_streaming_url = StreamingUrlModel::select('id')
					    ->where('video_episode_id', $video_episode_id)
					    ->where('url', $data_movies[$i]['anime_episodes'][$j]['anime_streaming'][0][$k])
					    ->first();

					    if($is_exist_streaming_url == null) {

					    	StreamingUrlModel::create([
					    		'video_episode_id' => $video_episode_id,
					    		'button_name' => $data_movies[$i]['anime_episodes'][$j]['anime_streaming'][1][$k],
					    		'url' => $data_movies[$i]['anime_episodes'][$j]['anime_streaming'][0][$k],
					    		'delete' => 0
					    	]);
					    }
					}
				}

				if(count($data_movies[$i]['anime_episodes'][$j]['anime_download'][0]) == count($data_movies[$i]['anime_episodes'][$j]['anime_download'][1])) {
				
					for ($l=0; $l < count($data_movies[$i]['anime_episodes'][$j]['anime_download'][0]); $l++) { 

						$is_exist_download_url = DownloadUrlModel::select('id')
					    ->where('video_episode_id', $video_episode_id)
					    ->where('url', $data_movies[$i]['anime_episodes'][$j]['anime_download'][0][$l])
					    ->first();

					    if($is_exist_download_url == null) {

					    	DownloadUrlModel::create([
					    		'video_episode_id' => $video_episode_id,
					    		'button_name' => $data_movies[$i]['anime_episodes'][$j]['anime_download'][1][$l],
					    		'url' => $data_movies[$i]['anime_episodes'][$j]['anime_download'][0][$l],
					    		'delete' => 0
					    	]);
					    }
					}
				}
			}
		}

		$result['status'] = '200';
    	
		return json_encode($result);
    }

    public function getRecentReleaseAnime()
    {
    	$requested = request()->validate([
    		'target_url' => 'required',
    		'base_url' => 'required'
    	]);


    	Log::info('isi url yang dikirim: '.$requested['target_url']);

    	// $target_url = 'https://ww4.gogoanime.io/page-recent-release.html?page=1';
    	$target_url = $requested['target_url'];
    	// $base_url = 'https://ww4.gogoanime.io';
    	$base_url = $requested['base_url'];

    	$plot_summary = "Plot Summary: ";
    	$plot_summary_len = strlen($plot_summary);

    	$released = "Released: ";
    	$released_len = strlen($released);

    	$ongoing_status = "Status: ";
    	$ongoing_status_len = strlen($ongoing_status);

    	$remove_category_string_for_slug = strlen("/category/");
    	$remove_category_string_for_slug = strlen("/category/");
    	$remove_sub_category_string_for_slug = strlen("/sub-category/");
    	$remove_choose_this_server_string = strlen("Choose this server");
    	$minus_remove_choose_this_server_string = $remove_choose_this_server_string * -1;

    	$result_anime_detail = [];
    	$result_anime_episode = [];
    	$result_anime_episode_streaming = [];
		$result_anime_episode_download = [];
    	$result_all_recent_release = [];

    	$recent_release_client = new Client();

    	$crawler_recent_release = $recent_release_client->request('GET', $target_url);

		$data_recent_releases = $crawler_recent_release->filter('.items .name a')->each(function ($node, $i) use($crawler_recent_release, $recent_release_client, $remove_sub_category_string_for_slug, $remove_category_string_for_slug, $minus_remove_choose_this_server_string, $base_url, $plot_summary_len, $released_len, $ongoing_status_len, $result_anime_detail, $result_anime_episode, $result_all_recent_release, $result_anime_episode_streaming, $result_anime_episode_download) {
	    
    		$anime_episode_detail_link = $crawler_recent_release->selectLink(trim($node->text()))->link();
			$crawler_anime_episode_detail = $recent_release_client->click($anime_episode_detail_link);

			// Anime Info
			$anime_info = $crawler_anime_episode_detail->filter('.anime-info a')->attr('title');
			Log::info('isi anime info: '.$anime_info);
			Log::info(' ');

			// Anime Info Slug
			$anime_info_slug = substr($crawler_anime_episode_detail->filter('.anime-info a')->attr('href'), $remove_category_string_for_slug);
			Log::info('isi anime info slug: '.$anime_info_slug);
			Log::info(' ');

			/* start get detaila anime */

			$detail_anime_client = new Client();

			$crawler_detail_anime = $detail_anime_client->request('GET', $base_url.'/category/'.$anime_info_slug);

	    	$anime_detail_slug = substr($crawler_detail_anime->filter('head link[rel=canonical]')->attr('href'), $remove_category_string_for_slug);
	    	$result_anime_detail['anime_detail_slug'] = $anime_detail_slug;

			$anime_detail_image = $crawler_detail_anime->filter('.anime_info_body_bg img')->attr('src');
			$result_anime_detail['anime_detail_image'] = $anime_detail_image;

			$anime_detail_title = $crawler_detail_anime->filter('.anime_info_body_bg h1')->each(function ($node) {
			    return $node->text();
			});
			$result_anime_detail['anime_detail_title'] = $anime_detail_title[0];

			$anime_detail_sub_category = $crawler_detail_anime->filter('.anime_info_body_bg a')->eq(1)->each(function ($node) {
			    return $node->text();
			});
			$result_anime_detail['anime_detail_sub_category'] = $anime_detail_sub_category[0];

			$anime_detail_sub_category_slug = substr($crawler_detail_anime->filter('.anime_info_body_bg a')->eq(1)->attr('href'), $remove_sub_category_string_for_slug);
			$result_anime_detail['anime_detail_sub_category_slug'] = $anime_detail_sub_category_slug;

			$anime_detail_description = $crawler_detail_anime->filter('.anime_info_body_bg p')->eq(2)->each(function ($node) use ($plot_summary_len) {
				return substr($node->text(), $plot_summary_len);
			});
			$result_anime_detail['anime_detail_description'] = $anime_detail_description[0];

			$anime_detail_genre = $crawler_detail_anime->filter('.anime_info_body_bg a')->extract(array('title'));

			unset($anime_detail_genre[0]);
			unset($anime_detail_genre[1]);

			$result_anime_detail['anime_detail_genre'] = $anime_detail_genre;

			$anime_detail_released = $crawler_detail_anime->filter('.anime_info_body_bg p')->eq(4)->each(function ($node) use ($released_len) {
				return substr($node->text(), $released_len);
			});
			$result_anime_detail['anime_detail_released'] = $anime_detail_released[0];

			$anime_detail_status = $crawler_detail_anime->filter('.anime_info_body_bg p')->eq(5)->each(function ($node) use ($ongoing_status_len) {
				return substr($node->text(), $ongoing_status_len);
			});
			$result_anime_detail['anime_detail_status'] = $anime_detail_status[0];

			$crawler_detail_anime->clear();
			unset($crawler_detail_anime);
			unset($detail_anime_client);
			
			/* end get detaila anime */

			// Title
			$anime_episode_title = $crawler_anime_episode_detail->filter('.title_name h2')->each(function ($node) {
				return $node->text();
			});

			$result_anime_episode['anime_episode_title'] = $anime_episode_title[0];

			// Category
			$anime_episode_category = $crawler_anime_episode_detail->filter('.anime_video_body_cate a')->attr('title');
			$result_anime_episode['anime_episode_category'] = $anime_episode_category;
			// Log::info('isi category: '.$category);
			// Log::info(' ');

			// Category Slug
			$anime_episode_category_slug = substr($crawler_anime_episode_detail->filter('.anime_video_body_cate a')->attr('href'), $remove_sub_category_string_for_slug);
			$result_anime_episode['anime_episode_category_slug'] = $anime_episode_category_slug;
			// Log::info('isi category slug: '.$category_slug);
			// Log::info(' ');

			// Anime Detail Slug
			$anime_episode_detail_slug = substr($crawler_anime_episode_detail->filter('head link[rel=canonical]')->attr('href'), 1);
			$result_anime_episode['anime_episode_detail_slug'] = $anime_episode_detail_slug;
			// Log::info('isi link anime detail: '.$anime_detail_slug);
			// Log::info(' ');

			// Streaming Links
			$streaming_links = $crawler_anime_episode_detail->filter('a[data-video]')->extract(array('data-video'));
			// Log::info('isi streaming links: '.json_encode($streaming_links));

			for ($i=0; $i < count($streaming_links); $i++) { 

			    $findme   = 'https:';
			    $pos = strpos($streaming_links[$i], $findme);

			    // Note our use of ===. Simply, == would not work as expected
			    // because the position of 'a' was the 0th (first) character.
			    if ($pos === false) {
			        $streaming_links[$i] = 'https:'.$streaming_links[$i];
			    }
			}

			// Streaming Link Names
			$streaming_link_names = $crawler_anime_episode_detail->filter('a[data-video]')->each(function ($node) use ($minus_remove_choose_this_server_string) {
			    return substr($node->text(), 0, $minus_remove_choose_this_server_string);
			});

			$result_anime_episode_streaming[0] = $streaming_links;
			$result_anime_episode_streaming[1] = $streaming_link_names;

			// Cari link dengan nama tombol download, lalu di pencet
			$link = $crawler_anime_episode_detail->selectLink('Download')->link();
			$crawler_download = $recent_release_client->click($link);

			// Download Links
			$download_links = $crawler_download->filter('.dowload a')->extract(array('href'));

			// Download Link Names
			$download_text_names = $crawler_download->filter('.dowload a')->each(function ($node) {
			    return $node->text();
			});

			$result_anime_episode_download[0] = $download_links;
			$result_anime_episode_download[1] = $download_text_names;

			$result_all_recent_release['anime_detail'] = $result_anime_detail;
			$result_all_recent_release['anime_episode_detail'] = $result_anime_episode;
			$result_all_recent_release['anime_episode_streaming'] = $result_anime_episode_streaming;
			$result_all_recent_release['anime_episode_download'] = $result_anime_episode_download;

			$crawler_anime_episode_detail->clear();
			unset($crawler_anime_episode_detail);

			$crawler_download->clear();
			unset($crawler_download);

			return $result_all_recent_release;
		});

		for ($i=0; $i < count($data_recent_releases); $i++) { 

			$video_id = '';

			$is_exist_video = VideoDescriptionModel::select('id')
			->where('slug', $data_recent_releases[$i]['anime_detail']['anime_detail_slug'])
			->where('delete', 0)
			->first();

			if($is_exist_video != null){
				$video_id = $is_exist_video->id;
			} else {
				$result_detail_anime = $get_video = VideoDescriptionModel::create([
					'user_id' => Auth::user()->id,
					'sub_category' => $data_recent_releases[$i]['anime_detail']['anime_detail_sub_category'],
					'sub_category_slug' => $data_recent_releases[$i]['anime_detail']['anime_detail_sub_category_slug'],
					'img' => $data_recent_releases[$i]['anime_detail']['anime_detail_image'],
					'slug' => $data_recent_releases[$i]['anime_detail']['anime_detail_slug'],
					'title' => $data_recent_releases[$i]['anime_detail']['anime_detail_title'],
					'description' => $data_recent_releases[$i]['anime_detail']['anime_detail_description'],
					'released' => $data_recent_releases[$i]['anime_detail']['anime_detail_released'],
					'status' => $data_recent_releases[$i]['anime_detail']['anime_detail_status'],
					'delete' => 0 
				]);

				$video_id = $result_detail_anime->id;
			}

			foreach ($data_recent_releases[$i]['anime_detail']['anime_detail_genre'] as $detail_anime_genre) {
				$is_exist_genre = ConnectVideoAndGenreModel::select('id')
				->where('video_description_id', $video_id)
				->where('genre_slug', str_slug($detail_anime_genre, '-'))
				->where('delete', 0)
				->first();

				if($is_exist_genre == null)
				{
					ConnectVideoAndGenreModel::create([
						'user_id' => Auth::user()->id,
						'video_description_id' => $video_id,
						'genre_slug' => str_slug($detail_anime_genre, '-'),
						'genre_text' => $detail_anime_genre,
						'delete' => 0
					]);
				}
			}

			$video_episode_id = '';

			$is_exist_video_episode = VideoEpisodeModel::select('GA_VideoEpisode.id')
			->where('video_description_id', $video_id)
			->where('slug', $data_recent_releases[$i]['anime_episode_detail']['anime_episode_detail_slug'])
			->where('delete', 0)
			->first();

			if($is_exist_video_episode != null) {
				$video_episode_id = $is_exist_video_episode->id;
			} else {
				$result_anime_episode_detail = VideoEpisodeModel::create([
					'video_description_id' => $video_id,
					'slug' => $data_recent_releases[$i]['anime_episode_detail']['anime_episode_detail_slug'],
					'title' => $data_recent_releases[$i]['anime_episode_detail']['anime_episode_title'],
					'delete' => 0
				]);

				$video_episode_id = $result_anime_episode_detail->id;
			}
			
			if(count($data_recent_releases[$i]['anime_episode_streaming'][0]) == count($data_recent_releases[$i]['anime_episode_streaming'][1])) {

				for ($k=0; $k < count($data_recent_releases[$i]['anime_episode_streaming'][0]); $k++) { 

					$is_exist_streaming_url = StreamingUrlModel::select('id')
				    ->where('video_episode_id', $video_episode_id)
				    ->where('url', $data_recent_releases[$i]['anime_episode_streaming'][0][$k])
				    ->first();

				    if($is_exist_streaming_url == null) {

				    	StreamingUrlModel::create([
				    		'video_episode_id' => $video_episode_id,
				    		'button_name' => $data_recent_releases[$i]['anime_episode_streaming'][1][$k],
				    		'url' => $data_recent_releases[$i]['anime_episode_streaming'][0][$k],
				    		'delete' => 0
				    	]);
				    }
				}
			}

			if(count($data_recent_releases[$i]['anime_episode_download'][0]) == count($data_recent_releases[$i]['anime_episode_download'][1])) {
				
				for ($j=0; $j < count($data_recent_releases[$i]['anime_episode_download'][0]); $j++) { 

					$is_exist_download_url = DownloadUrlModel::select('id')
				    ->where('video_episode_id', $video_episode_id)
				    ->where('url', $data_recent_releases[$i]['anime_episode_download'][0][$j])
				    ->first();

				    if($is_exist_download_url == null) {

				    	DownloadUrlModel::create([
				    		'video_episode_id' => $video_episode_id,
				    		'button_name' => $data_recent_releases[$i]['anime_episode_download'][1][$j],
				    		'url' => $data_recent_releases[$i]['anime_episode_download'][0][$j],
				    		'delete' => 0
				    	]);
				    }
				}
			}
		}

		$crawler_recent_release->clear();
		unset($crawler_recent_release);
		unset($recent_release_client);

    	$result['status'] = '200';
    	
		return json_encode($result);
    }

    public function getAnime()
    {
    	$requested = request()->validate([
    		'target_url' => 'required',
    		'base_url' => 'required',
    		'ep_start' => 'required',
    		'ep_end' => 'required'
    	]);

    	Log::info('isi target url: '.$requested['target_url']);
    	Log::info('isi ep start: '.$requested['ep_start']);
    	Log::info('isi ep end: '.$requested['ep_end']);


	    $base_url = $requested['target_url'];
	    // $base_url = 'https://ww4.gogoanime.io/category/shokugeki-no-souma-san-no-sara';
    	$plot_summary = "Plot Summary: ";
    	$plot_summary_len = strlen($plot_summary);

    	$released = "Released: ";
    	$released_len = strlen($released);

    	$ongoing_status = "Status: ";
    	$ongoing_status_len = strlen($ongoing_status);

    	$remove_category_string_for_slug = strlen("/category/");
    	$remove_category_string_for_slug = strlen("/category/");
    	$remove_sub_category_string_for_slug = strlen("/sub-category/");
    	$remove_choose_this_server_string = strlen("Choose this server");
    	$minus_remove_choose_this_server_string = $remove_choose_this_server_string * -1;

    	$url = $base_url;

    	$client = new Client();

    	$crawler = $client->request('GET', $url);

    	// Nge get cuma 1 biji
    	$anime_slug = substr($crawler->filter('head link[rel=canonical]')->attr('href'), $remove_category_string_for_slug);
		// print_r("Slug: ".$anime_slug);

		// print "\n";

		$anime_image = $crawler->filter('.anime_info_body_bg img')->attr('src');
		// print_r($anime_image);

		// print "\n";

		$anime_title = $crawler->filter('.anime_info_body_bg h1')->each(function ($node) {
		    return $node->text();
		});

		// print($anime_title[0]);

		// print "\n";

		$anime_sub_category = $crawler->filter('.anime_info_body_bg a')->eq(1)->each(function ($node) {
		    return $node->text();
		});

		$anime_sub_category_slug = substr($crawler->filter('.anime_info_body_bg a')->eq(1)->attr('href'), $remove_sub_category_string_for_slug);

		// print($anime_sub_category[0]);

		// print "\n";

		$anime_description = $crawler->filter('.anime_info_body_bg p')->eq(2)->each(function ($node) use ($plot_summary_len) {
			return substr($node->text(), $plot_summary_len);
		});

		// print($anime_description[0]);

		// print "\n";

		$anime_released = $crawler->filter('.anime_info_body_bg p')->eq(4)->each(function ($node) use ($released_len) {
			return substr($node->text(), $released_len);
		});

		// print($anime_released[0]);

		// print "\n";

		$anime_status = $crawler->filter('.anime_info_body_bg p')->eq(5)->each(function ($node) use ($ongoing_status_len) {
			return substr($node->text(), $ongoing_status_len);
		});

		// print($anime_status[0]);

		$anime_genre_texts = $crawler->filter('.anime_info_body_bg a')->extract(array('title'));
		// print_r($crawler->filter('.anime_info_body_bg a')->extract(array('title')));

		unset($anime_genre_texts[0]);
		unset($anime_genre_texts[1]);

		$ep_start = $requested['ep_start'];
		// $ep_start = 1;
		$ep_end = $requested['ep_end'];
		// $ep_end = 11;
		$movie_id = $crawler->filter('input#movie_id')->attr('value');

		$result_all_video_episode = [];
		$result_video_episode = [];
		$result_video_streaming = [];
		$result_video_download = [];

		$get_load_list_episode_url = $requested['base_url'].'/load-list-episode?ep_start='.$ep_start.'&ep_end='.$ep_end.'&id='.$movie_id;
		// $get_load_list_episode_url = 'https://ww4.gogoanime.io'.'/load-list-episode?ep_start='.$ep_start.'&ep_end='.$ep_end.'&id='.$movie_id;
		// Log::info($get_load_list_episode_url);
		$get_episode_lists = new Client();

    	$crawler_episode_lists = $get_episode_lists->request('GET', $get_load_list_episode_url);

    	$data_video_episodes = $crawler_episode_lists->filter('li .name')->each(function ($node, $i) use ($crawler_episode_lists, $get_episode_lists, $remove_sub_category_string_for_slug, $remove_category_string_for_slug, $minus_remove_choose_this_server_string, $result_all_video_episode, $result_video_episode, $result_video_streaming, $result_video_download) {

			$episode_links = $crawler_episode_lists->selectLink(trim($node->text()))->link();
			$crawler_episode_links = $get_episode_lists->click($episode_links);

			// Title
			$anime_title = $crawler_episode_links->filter('.title_name h2')->each(function ($node) {
				return $node->text();
			});
			$result_video_episode['anime_title'] = $anime_title[0];
			// print_r($title[0]);

			// Sub Category
			$sub_category = $crawler_episode_links->filter('.anime_video_body_cate a')->attr('title');
			// print_r($sub_category);
			$result_video_episode['sub_category'] = $sub_category;

			// Sub Category Slug
			$sub_category_slug = substr($crawler_episode_links->filter('.anime_video_body_cate a')->attr('href'), $remove_sub_category_string_for_slug);
			// print_r($sub_category_slug);
			$result_video_episode['sub_category_slug'] = $sub_category_slug;


			// Anime Detail Slug
			$anime_detail_slug = substr($crawler_episode_links->filter('head link[rel=canonical]')->attr('href'), 1);
			// print_r($anime_detail_slug);
			$result_video_episode['anime_detail_slug'] = $anime_detail_slug;

			// Anime Info
			$anime_info = $crawler_episode_links->filter('.anime-info a')->attr('title');
			// print_r($anime_info);
			$result_video_episode['anime_info'] = $anime_info;

			// Anime Info Slug
			$anime_info_slug = substr($crawler_episode_links->filter('.anime-info a')->attr('href'), $remove_category_string_for_slug);
			// print_r($anime_info_slug);
			$result_video_episode['anime_info_slug'] = $anime_info_slug;

			$streaming_links = $crawler_episode_links->filter('a[data-video]')->extract(array('data-video'));
			// print_r($streaming_links);

			for ($i=0; $i < count($streaming_links); $i++) { 

			    $findme   = 'https:';
			    $pos = strpos($streaming_links[$i], $findme);

			    // Note our use of ===. Simply, == would not work as expected
			    // because the position of 'a' was the 0th (first) character.
			    if ($pos === false) {
			        $streaming_links[$i] = 'https:'.$streaming_links[$i];
			    }
			}

			// Streaming Link Names
			$streaming_link_names = $crawler_episode_links->filter('a[data-video]')->each(function ($node) use ($minus_remove_choose_this_server_string) {
			    return substr($node->text(), 0, $minus_remove_choose_this_server_string);
			});

			$result_video_streaming[0] = $streaming_links;
			$result_video_streaming[1] = $streaming_link_names;
			
			$link = $crawler_episode_links->selectLink('Download')->link();
			$crawler_download = $get_episode_lists->click($link);

			// Download Links
			$download_links = $crawler_download->filter('.dowload a')->extract(array('href'));

			// print_r($download_links);

			// Download Link Names
			$download_text_names = $crawler_download->filter('.dowload a')->each(function ($node) {
			    return $node->text();
			});

			$crawler_download->clear();
			unset($crawler_download);
			$crawler_episode_links->clear();
			unset($crawler_episode_links);

			$result_video_download[0] = $download_links;
			$result_video_download[1] = $download_text_names;

			$result_all_video_episode['anime_detail'] = $result_video_episode;
			$result_all_video_episode['anime_streaming'] = $result_video_streaming;
			$result_all_video_episode['anime_download'] = $result_video_download;

			return $result_all_video_episode;
		});

    	$crawler_episode_lists->clear();
    	unset($crawler_episode_lists);
    	$crawler->clear();
    	unset($crawler);
    	unset($client);

    	$page_access_token = '315294018960732|XCJxYeUBFzgtT2KkdujidmUiEH8';
		$page_id = '245705372149003';
		$data = [];

    	$video_id = '';

    	$is_exist_video = VideoDescriptionModel::select('id')
		->where('slug', $anime_slug)
		->where('delete', 0)
		->first();

		if($is_exist_video != null){
			$video_id = $is_exist_video->id;
		} else {
			$result_video = $get_video = VideoDescriptionModel::create([
				'user_id' => Auth::user()->id,
				'sub_category' => $anime_sub_category[0],
				'sub_category_slug' => $anime_sub_category_slug,
				'img' => $anime_image,
				'slug' => $anime_slug,
				'title' => $anime_title[0],
				'description' => $anime_description[0],
				'released' => $anime_released[0],
				'status' => $anime_status[0],
				'delete' => 0 
			]);

			$video_id = $result_video->id;
			// $data['picture'] = $anime_image;
		}

		foreach ($anime_genre_texts as $anime_genre_text) 
		{
			$is_exist_genre = ConnectVideoAndGenreModel::select('id')
			->where('video_description_id', $video_id)
			->where('genre_slug', str_slug($anime_genre_text, '-'))
			->where('delete', 0)
			->first();

			if($is_exist_genre == null)
			{
				ConnectVideoAndGenreModel::create([
					'user_id' => Auth::user()->id,
					'video_description_id' => $video_id,
					'genre_slug' => str_slug($anime_genre_text, '-'),
					'genre_text' => $anime_genre_text,
					'delete' => 0
				]);
			}
		}
    	
    	$video_episode_id = '';

    	foreach ($data_video_episodes as $data_video_episode) 
    	{
    		$is_exist_video_episode = VideoEpisodeModel::select('GA_VideoEpisode.id')
			->where('video_description_id', $video_id)
			->where('slug', $data_video_episode['anime_detail']['anime_detail_slug'])
			->where('delete', 0)
			->first();

			if($is_exist_video_episode != null) {
				$video_episode_id = $is_exist_video_episode->id;
			} else {
				$result_video_episode = VideoEpisodeModel::create([
					'video_description_id' => $video_id,
					'slug' => $data_video_episode['anime_detail']['anime_detail_slug'],
					'title' => $data_video_episode['anime_detail']['anime_title'],
					'delete' => 0
				]);

				$video_episode_id = $result_video_episode->id;

				// $data['link'] = "https://ganime.arthatransindo.com/";
				// $data['message'] = "Hi guys, ".$data_video_episode['anime_detail']['anime_title']." is out now ! ";
				// $data['access_token'] = $page_access_token;
				// $post_url = 'https://graph.facebook.com/'.$page_id.'/feed';

				// $ch = curl_init();
				// curl_setopt($ch, CURLOPT_URL, $post_url);
				// curl_setopt($ch, CURLOPT_POST, 1);
				// curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
				// curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				// $return = curl_exec($ch);
				// curl_close($ch);
			}

			if(count($data_video_episode['anime_streaming'][0]) == count($data_video_episode['anime_streaming'][1])) {

				for ($i=0; $i < count($data_video_episode['anime_streaming'][0]); $i++) { 

					$is_exist_streaming_url = StreamingUrlModel::select('id')
				    ->where('video_episode_id', $video_episode_id)
				    ->where('url', $data_video_episode['anime_streaming'][0][$i])
				    ->first();

				    if($is_exist_streaming_url == null) {

				    	StreamingUrlModel::create([
				    		'video_episode_id' => $video_episode_id,
				    		'button_name' => $data_video_episode['anime_streaming'][1][$i],
				    		'url' => $data_video_episode['anime_streaming'][0][$i],
				    		'delete' => 0
				    	]);
				    }
				}
			}

			if(count($data_video_episode['anime_download'][0]) == count($data_video_episode['anime_download'][1])) {
				
				for ($i=0; $i < count($data_video_episode['anime_download'][0]); $i++) { 

					$is_exist_download_url = DownloadUrlModel::select('id')
				    ->where('video_episode_id', $video_episode_id)
				    ->where('url', $data_video_episode['anime_download'][0][$i])
				    ->first();

				    if($is_exist_download_url == null) {

				    	DownloadUrlModel::create([
				    		'video_episode_id' => $video_episode_id,
				    		'button_name' => $data_video_episode['anime_download'][1][$i],
				    		'url' => $data_video_episode['anime_download'][0][$i],
				    		'delete' => 0
				    	]);
				    }
				}
			}
    	}

    	// https://ww3.gogoanime.io/load-list-episode?ep_start=2&ep_end=5&id=133

    	$result['status'] = '200';

		return json_encode($result);
    }

    public function getDetailAnime()
    {
    	$remove_category_string_for_slug = strlen("/category/");
    	$remove_sub_category_string_for_slug = strlen("/sub-category/");
    	$remove_choose_this_server_string = strlen("Choose this server");
    	$minus_remove_choose_this_server_string = $remove_choose_this_server_string * -1;

    	$client = new Client();

    	$crawler = $client->request('GET', 'https://ww3.gogoanime.io/black-clover-tv-episode-5');

    	// Title
		$title = $crawler->filter('.title_name h2')->each(function ($node) {
			return $node->text();
		});

		// print_r($title[0]);

		// print "\n";

		// Category
		$category = $crawler->filter('.anime_video_body_cate a')->attr('title');
		// print_r($category);

		// print "\n";

		// Category Slug
		$category_slug = substr($crawler->filter('.anime_video_body_cate a')->attr('href'), $remove_sub_category_string_for_slug);
		// print_r($category_slug);

		// print "\n";

		// Anime Detail Slug
		$anime_detail_slug = substr($crawler->filter('head link[rel=canonical]')->attr('href'), 1);
		// print_r($anime_detail_slug);

		// print "\n";

		// Anime Info
		$anime_info = $crawler->filter('.anime-info a')->attr('title');
		// print_r($anime_info);

		// print "\n";

		// Anime Info Slug
		$anime_info_slug = substr($crawler->filter('.anime-info a')->attr('href'), $remove_category_string_for_slug);
		// print_r($anime_info_slug);

		// print "\n";

		// Streaming Links
		$streaming_links = $crawler->filter('a[data-video]')->extract(array('data-video'));
		// print_r($streaming_links);

		// Streaming Link Names
		$streaming_link_names = $crawler->filter('a[data-video]')->each(function ($node) use ($minus_remove_choose_this_server_string) {
		    return substr($node->text(), 0, $minus_remove_choose_this_server_string);
		});
		// print_r($streaming_link_names);

		// Cari link dengan nama tombol download, lalu di pencet
		$link = $crawler->selectLink('Download')->link();
		$crawler_download = $client->click($link);

		// Download Links
		$download_links = $crawler_download->filter('.dowload a')->extract(array('href'));

		// print_r($download_links);

		// print "\n";

		// Download Link Names
		$download_text_names = $crawler_download->filter('.dowload a')->each(function ($node) {
		    return $node->text();
		});

		print_r($download_text_names);
		// foreach ($download_text_names as $download_text_name) 
		// {
		// 	print($download_text_name);
		// 	print "\n";
		// }

    }

    public function penjelasan_fungsi_dom_crawler()
    {
    	// filter dengan class nya dan html tag h2, lalu ambil value dari h2 nya
		$title = $crawler->filter('.title_name h2')->each(function ($node) {
		    print "Title: ".$node->text()."\n";
			return $node->text();
		});

    	// Nge get cuma 1 biji, filter class nya lalu ambil attribute value dengan yang ada title nya
		$anime_info = $crawler->filter('.anime-info a')->attr('title');
		print_r("Anime Info: ".$anime_info);

		// Ambil array ke 1 dari result
		$crawler->filter('.anime_info_body_bg a')->eq(1)->each(function ($node) {
		    print $node->text()."\n";
		});

		// hasil result array dari isi genre, lalu array ke 0 dan 1 di buang, lalu di foreach array nya
		$isi_genre = $crawler->filter('.anime_info_body_bg a')->extract(array('title'));
		// print_r($crawler->filter('.anime_info_body_bg a')->extract(array('title')));

		unset($isi_genre[0]);
		unset($isi_genre[1]);

		print_r($isi_genre);
		print "\n";

		foreach ($isi_genre as $genre) 
		{
			print($genre);
			print "\n";
		}
    }
}