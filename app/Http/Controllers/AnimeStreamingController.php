<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\VideoEpisodeModel;
use App\StreamingUrlModel;
use Auth, Hash, DB, Log;

class AnimeStreamingController extends Controller
{
    public function __construct(VideoEpisodeModel $videoEpisodeModel, StreamingUrlModel $streamingUrlModel)
    {
        $this->videoEpisodeModel = $videoEpisodeModel;
        $this->streamingUrlModel = $streamingUrlModel;
        DB::enableQueryLog();
    }

    public function getAnimeStreaming($anime_episode_id)
    {
    	$anime_episode = $this->videoEpisodeModel->getOneAnimeEpisodeWithAnimeEpisodeIdOnly($anime_episode_id);

    	if($anime_episode == null)
    	{
    		return redirect()->route('getAnime');
    	}
    	
        return view('admin/anime-streaming', ['user' => Auth::user(), 'anime_episode_id' => $anime_episode_id, 'anime_episode' => $anime_episode]);
    }

    public function postAjaxAnimeStreaming(Request $request, $anime_episode_id)
    {
        $data = array();

        $columns = array( 
            0 => 'id', 
            1 => 'button_name',
            2 => 'url',
            3 => 'id'
        );
  
        $totalData = $this->streamingUrlModel->countAllActiveAnimeStreaming($anime_episode_id);
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value'); 

        if(empty($search))
        {            
            $anime_streamings = $this->streamingUrlModel->getAnimeStreaming($start, $limit, $order, $dir, $anime_episode_id);
        }
        else 
        {
            $anime_streamings = $this->streamingUrlModel->getFilteredAnimeStreaming($search, $start, $limit, $order, $dir, $anime_episode_id);
            $totalFiltered = $this->streamingUrlModel->countAllFilteredActiveAnimeStreaming($search, $anime_episode_id);
        }

        if(!empty($anime_streamings))
        {
            foreach ($anime_streamings as $anime_streaming)
            {
                $edit =  url('/admin-access/edit-anime-streaming/'.$anime_streaming->id.'/with-anime-episode-id/'.$anime_episode_id);
                $delete =  url('/admin-access/delete-anime-streaming/'.$anime_streaming->id.'/with-anime-episode-id/'.$anime_episode_id);
                $callback_delete = url('/admin-access/anime-streaming-with-anime-episode-id/'.$anime_episode_id);

                $nestedData['id'] = $anime_streaming->id;
                $nestedData['button_name'] = $anime_streaming->button_name;
                $nestedData['url'] = $anime_streaming->url;

                $nestedData['action_btn'] = "
                    <button onclick='master_edit(\"".$edit."\")' type='button' class='btn btn-info mr-1 mb-1'><i class='ft-edit'></i></button>
                    <button onclick='master_delete(\"".$delete."\", \"".$callback_delete."\")' type='button' class='btn btn-danger mr-1 mb-1'><i class='ft-trash-2'></i></button>
                ";
                
                $data[] = $nestedData;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function getAddAnimeStreaming($anime_episode_id)
    {
    	$anime_episode = $this->videoEpisodeModel->getOneAnimeEpisodeWithAnimeEpisodeIdOnly($anime_episode_id);

    	if($anime_episode == null)
    	{
    		return redirect()->route('getAnime');
    	}

    	return view('admin/add-anime-streaming', ['user' => Auth::user(), 'anime_episode_id' => $anime_episode_id, 'anime_episode' => $anime_episode]);
    }

    public function postAddAnimeStreaming($anime_episode_id)
    {
    	$requested = request()->validate([
    		'button_name' => 'required',
    		'url' => 'required'
    	]);

    	$anime_episode = $this->videoEpisodeModel->getOneAnimeEpisodeWithAnimeEpisodeIdOnly($anime_episode_id);

    	if($anime_episode == null)
    	{
    		return redirect()->route('getAnime');
    	}

    	$result = $this->streamingUrlModel->postAddAnimeStreaming($requested, $anime_episode_id);
    	
    	return redirect()->route('getEditAnimeStreaming', ['id' => $result[0], 'anime_episode_id' => $anime_episode_id])->with(['done' => $result[1]] );
    }

    public function getEditAnimeStreaming($id, $anime_episode_id)
    {
    	$anime_streaming = $this->streamingUrlModel->getOneAnimeStreaming($id, $anime_episode_id);

    	if($anime_streaming == null)
    	{
    		return redirect()->route('getAnime');
    	}

    	$anime_episode = $this->videoEpisodeModel->getOneAnimeEpisodeWithAnimeEpisodeIdOnly($anime_episode_id);

    	if($anime_episode == null)
    	{
    		return redirect()->route('getAnime');
    	}

    	return view('admin/edit-anime-streaming', ['user' => Auth::user(), 'anime_streaming' => $anime_streaming, 'anime_episode_id' => $anime_episode_id, 'anime_episode' => $anime_episode]);
    }

    public function postEditAnimeStreaming($id, $anime_episode_id)
    {
    	$anime_streaming = $this->streamingUrlModel->getOneAnimeStreaming($id, $anime_episode_id);

    	if($anime_streaming == null)
    	{
    		return redirect()->route('getAnime');
    	}

    	$requested = request()->validate([
    		'button_name' => 'required',
    		'url' => 'required'
    	]);

    	$result = $this->streamingUrlModel->postEditAnimeStreaming($requested, $id, $anime_episode_id);

    	return redirect()->route('getEditAnimeStreaming', ['id' => $id, 'anime_episode_id' => $anime_episode_id])->with(['done' => $result] );
    }

    public function postDeleteAnimeStreaming($id, $anime_episode_id)
    {
    	$anime_streaming = $this->streamingUrlModel->getOneAnimeStreaming($id, $anime_episode_id);

    	if($anime_streaming == null)
    	{
    		return redirect()->route('getAnime');
    	}

    	$result = $this->streamingUrlModel->postDeleteAnimeStreaming($id, $anime_episode_id);

    	return json_encode($result);
    }
}
