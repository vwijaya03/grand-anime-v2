<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserModel;
use Auth, Hash, DB, Log;

class AuthorizedRolesController extends Controller
{
    public static function authorizedRoles($authorizedRoles, $current_user_role)
	{
		$user_role = [];
		$user_role[] = $current_user_role;

		$diff = array_diff($authorizedRoles, $user_role);

		if(count($diff) > 0) {
			return abort(401, 'This action is unauthorized.');
		}
	}
}
