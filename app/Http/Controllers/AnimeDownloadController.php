<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\VideoEpisodeModel;
use App\DownloadUrlModel;
use Auth, Hash, DB, Log;

class AnimeDownloadController extends Controller
{
    public function __construct(VideoEpisodeModel $videoEpisodeModel, DownloadUrlModel $downloadUrlModel)
    {
        $this->videoEpisodeModel = $videoEpisodeModel;
        $this->downloadUrlModel = $downloadUrlModel;
        DB::enableQueryLog();
    }

    public function getAnimeDownload($anime_episode_id)
    {
    	$anime_episode = $this->videoEpisodeModel->getOneAnimeEpisodeWithAnimeEpisodeIdOnly($anime_episode_id);

    	if($anime_episode == null)
    	{
    		return redirect()->route('getAnime');
    	}
    	
        return view('admin/anime-download', ['user' => Auth::user(), 'anime_episode_id' => $anime_episode_id, 'anime_episode' => $anime_episode]);
    }

    public function postAjaxAnimeDownload(Request $request, $anime_episode_id)
    {
        $data = array();

        $columns = array( 
            0 => 'id', 
            1 => 'button_name',
            2 => 'url',
            3 => 'id'
        );
  
        $totalData = $this->downloadUrlModel->countAllActiveAnimeDownload($anime_episode_id);
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value'); 

        if(empty($search))
        {            
            $anime_downloads = $this->downloadUrlModel->getAnimeDownload($start, $limit, $order, $dir, $anime_episode_id);
        }
        else 
        {
            $anime_downloads = $this->downloadUrlModel->getFilteredAnimeDownload($search, $start, $limit, $order, $dir, $anime_episode_id);
            $totalFiltered = $this->downloadUrlModel->countAllFilteredActiveAnimeDownload($search, $anime_episode_id);
        }

        if(!empty($anime_downloads))
        {
            foreach ($anime_downloads as $anime_download)
            {
                $edit =  url('/admin-access/edit-anime-download/'.$anime_download->id.'/with-anime-episode-id/'.$anime_episode_id);
                $delete =  url('/admin-access/delete-anime-download/'.$anime_download->id.'/with-anime-episode-id/'.$anime_episode_id);
                $callback_delete = url('/admin-access/anime-download-with-anime-episode-id/'.$anime_episode_id);

                $nestedData['id'] = $anime_download->id;
                $nestedData['button_name'] = $anime_download->button_name;
                $nestedData['url'] = $anime_download->url;

                $nestedData['action_btn'] = "
                    <button onclick='master_edit(\"".$edit."\")' type='button' class='btn btn-info mr-1 mb-1'><i class='ft-edit'></i></button>
                    <button onclick='master_delete(\"".$delete."\", \"".$callback_delete."\")' type='button' class='btn btn-danger mr-1 mb-1'><i class='ft-trash-2'></i></button>
                ";
                
                $data[] = $nestedData;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function getAddAnimeDownload($anime_episode_id)
    {
    	$anime_episode = $this->videoEpisodeModel->getOneAnimeEpisodeWithAnimeEpisodeIdOnly($anime_episode_id);

    	if($anime_episode == null)
    	{
    		return redirect()->route('getAnime');
    	}

    	return view('admin/add-anime-download', ['user' => Auth::user(), 'anime_episode_id' => $anime_episode_id, 'anime_episode' => $anime_episode]);
    }

    public function postAddAnimeDownload($anime_episode_id)
    {
    	$requested = request()->validate([
    		'button_name' => 'required',
    		'url' => 'required'
    	]);

    	$anime_episode = $this->videoEpisodeModel->getOneAnimeEpisodeWithAnimeEpisodeIdOnly($anime_episode_id);

    	if($anime_episode == null)
    	{
    		return redirect()->route('getAnime');
    	}

    	$result = $this->downloadUrlModel->postAddAnimeDownload($requested, $anime_episode_id);
    	
    	return redirect()->route('getEditAnimeDownload', ['id' => $result[0], 'anime_episode_id' => $anime_episode_id])->with(['done' => $result[1]] );
    }

    public function getEditAnimeDownload($id, $anime_episode_id)
    {
    	$anime_download = $this->downloadUrlModel->getOneAnimeDownload($id, $anime_episode_id);

    	if($anime_download == null)
    	{
    		return redirect()->route('getAnime');
    	}

    	$anime_episode = $this->videoEpisodeModel->getOneAnimeEpisodeWithAnimeEpisodeIdOnly($anime_episode_id);

    	if($anime_episode == null)
    	{
    		return redirect()->route('getAnime');
    	}

    	return view('admin/edit-anime-download', ['user' => Auth::user(), 'anime_download' => $anime_download, 'anime_episode_id' => $anime_episode_id, 'anime_episode' => $anime_episode]);
    }

    public function postEditAnimeDownload($id, $anime_episode_id)
    {
    	$anime_download = $this->downloadUrlModel->getOneAnimeDownload($id, $anime_episode_id);

    	if($anime_download == null)
    	{
    		return redirect()->route('getAnime');
    	}

    	$requested = request()->validate([
    		'button_name' => 'required',
    		'url' => 'required'
    	]);

    	$result = $this->downloadUrlModel->postEditAnimeDownload($requested, $id, $anime_episode_id);

    	return redirect()->route('getEditAnimeDownload', ['id' => $id, 'anime_episode_id' => $anime_episode_id])->with(['done' => $result] );
    }

    public function postDeleteAnimeDownload($id, $anime_episode_id)
    {
    	$anime_download = $this->downloadUrlModel->getOneAnimeDownload($id, $anime_episode_id);

    	if($anime_download == null)
    	{
    		return redirect()->route('getAnime');
    	}

    	$result = $this->downloadUrlModel->postDeleteAnimeDownload($id, $anime_episode_id);

    	return json_encode($result);
    }
}
