<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserModel;
use Auth, Hash, DB, Log;

class UserController extends Controller
{
    public function __construct(UserModel $userModel)
    {
        $this->userModel = $userModel;
        $this->page_title_admin = 'Admin';
        $this->page_title_customer = 'Customer';
        $this->page_title_user = 'User';
        $this->role = '';
        DB::enableQueryLog();
    }

    public function getAdmin()
    {
        return view('admin/admin', ['user' => Auth::user(), 'page_title' => $this->page_title_admin]);
    }

    public function postAjaxAdmin(Request $request)
    {
        $data = array();

        $columns = array( 
            0 => 'id', 
            1 => 'name',
            2 => 'email',
            3 => 'role',
            4 => 'id'
        );
  
        $totalData = $this->userModel->countAllActiveUser($this->role = 'admin');
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value'); 

        if(empty($search))
        {            
            $admins = $this->userModel->getUser($start, $limit, $order, $dir, $this->role = 'admin');
        }
        else 
        {
            $admins = $this->userModel->getFilteredUser($search, $start, $limit, $order, $dir, $this->role = 'admin');
            $totalFiltered = $this->userModel->countAllFilteredActiveUser($search, $this->role = 'admin');
        }

        if(!empty($admins))
        {
            foreach ($admins as $admin)
            {
                $edit =  url('/admin-access/edit-admin/'.$admin->id);
                $delete =  url('/admin-access/delete-admin/'.$admin->id);

                $nestedData['id'] = $admin->id;
                $nestedData['name'] = $admin->name;
                $nestedData['email'] = $admin->email;
                $nestedData['role'] = $admin->role;

                $nestedData['action_btn'] = "
                    <button onclick='master_edit(\"".$edit."\")' type='button' class='btn btn-info mr-1 mb-1'><i class='ft-edit'></i></button>
                    <button onclick='master_delete(\"".$delete."\", \"".'admin'."\")' type='button' class='btn btn-danger mr-1 mb-1'><i class='ft-trash-2'></i></button>
                ";
                
                $data[] = $nestedData;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function getAddAdmin()
    {
    	return view('admin/add-admin', ['user' => Auth::user(), 'page_title' => $this->page_title_admin]);
    }

    public function postAddAdmin()
    {
    	$requested = request()->validate([
    		'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'role' => 'required'
    	]);

    	$result = $this->userModel->postAddUser($requested);
    	
    	return redirect()->route('getEditAdmin', ['id' => $result[0]])->with(['done' => $result[1]] );
    }

    public function getEditAdmin($id)
    {
    	$admin = $this->userModel->getOneUser($id, $this->role = 'admin');

    	if($admin == null)
    	{
    		return redirect()->route('getAdmin');
    	}

    	return view('admin/edit-admin', ['user' => Auth::user(), 'admin' => $admin, 'page_title' => $this->page_title_admin]);
    }

    public function postEditAdmin($id)
    {
    	$admin = $this->userModel->getOneUser($id, $this->role = 'admin');

    	if($admin == null)
    	{
    		return redirect()->route('getAdmin');
    	}

    	$requested = request()->validate([
    		'name' => 'required',
            'email' => 'required',
            'password' => '',
            'role' => 'required'
    	]);

    	$result = $this->userModel->postEditUser($requested, $id);

    	return redirect()->route('getEditAdmin', ['id' => $id])->with(['done' => $result] );
    }

    public function postDeleteAdmin($id)
    {
    	$admin = $this->userModel->getOneUser($id, $this->role = 'admin');

    	if($admin == null)
    	{
    		return redirect()->route('getAdmin');
    	}

    	$result = $this->userModel->postDeleteUser($id);

    	return json_encode($result);
    }

    /* ------------------------------------- CUSTOMER --------------------------------------- */

    public function getCustomer()
    {
        return view('admin/customer', ['user' => Auth::user(), 'page_title' => $this->page_title_customer]);
    }

    public function postAjaxCustomer(Request $request)
    {
        $data = array();

        $columns = array( 
            0 => 'id', 
            1 => 'name',
            2 => 'email',
            3 => 'role',
            4 => 'id'
        );
  
        $totalData = $this->userModel->countAllActiveUser($this->role = 'customer');
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value'); 

        if(empty($search))
        {            
            $customers = $this->userModel->getUser($start, $limit, $order, $dir, $this->role = 'customer');
        }
        else 
        {
            $customers = $this->userModel->getFilteredUser($search, $start, $limit, $order, $dir, $this->role = 'customer');
            $totalFiltered = $this->userModel->countAllFilteredActiveUser($search, $this->role = 'customer');
        }

        if(!empty($customers))
        {
            foreach ($customers as $customer)
            {
                $edit =  url('/admin-access/edit-customer/'.$customer->id);
                $delete =  url('/admin-access/delete-customer/'.$customer->id);

                $nestedData['id'] = $customer->id;
                $nestedData['name'] = $customer->name;
                $nestedData['email'] = $customer->email;
                $nestedData['role'] = $customer->role;

                $nestedData['action_btn'] = "
                    <button onclick='master_edit(\"".$edit."\")' type='button' class='btn btn-info mr-1 mb-1'><i class='ft-edit'></i></button>
                    <button onclick='master_delete(\"".$delete."\", \"".'customer'."\")' type='button' class='btn btn-danger mr-1 mb-1'><i class='ft-trash-2'></i></button>
                ";
                
                $data[] = $nestedData;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function getAddCustomer()
    {
    	return view('admin/add-customer', ['user' => Auth::user(), 'page_title' => $this->page_title_customer]);
    }

    public function postAddCustomer()
    {
    	$requested = request()->validate([
    		'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'role' => 'required'
    	]);

    	$result = $this->userModel->postAddUser($requested);
    	
    	return redirect()->route('getEditCustomer', ['id' => $result[0]])->with(['done' => $result[1]] );
    }

    public function getEditCustomer($id)
    {
    	$customer = $this->userModel->getOneUser($id, $this->role = 'customer');

    	if($customer == null)
    	{
    		return redirect()->route('getCustomer');
    	}

    	return view('admin/edit-customer', ['user' => Auth::user(), 'customer' => $customer, 'page_title' => $this->page_title_customer]);
    }

    public function postEditCustomer($id)
    {
    	$customer = $this->userModel->getOneUser($id, $this->role = 'customer');

    	if($customer == null)
    	{
    		return redirect()->route('getCustomer');
    	}

    	$requested = request()->validate([
    		'name' => 'required',
            'email' => 'required',
            'password' => '',
            'role' => 'required'
    	]);

    	$result = $this->userModel->postEditUser($requested, $id);

    	return redirect()->route('getEditCustomer', ['id' => $id])->with(['done' => $result] );
    }

    public function postDeleteCustomer($id)
    {
    	$customer = $this->userModel->getOneUser($id, $this->role = 'customer');

    	if($customer == null)
    	{
    		return redirect()->route('getCustomer');
    	}

    	$result = $this->userModel->postDeleteUser($id);

    	return json_encode($result);
    }

    /* ------------------------------------- ALL USER --------------------------------------- */

    public function getUser()
    {
        return view('admin/user', ['user' => Auth::user(), 'page_title' => $this->page_title_user]);
    }

    public function postAjaxUser(Request $request)
    {
        $data = array();

        $columns = array( 
            0 => 'id', 
            1 => 'name',
            2 => 'email',
            3 => 'role',
            4 => 'id'
        );
  
        $totalData = $this->userModel->countAllActiveUser($this->role);
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value'); 

        if(empty($search))
        {            
            $users = $this->userModel->getUser($start, $limit, $order, $dir, $this->role);
        }
        else 
        {
            $users = $this->userModel->getFilteredUser($search, $start, $limit, $order, $dir, $this->role);
            $totalFiltered = $this->userModel->countAllFilteredActiveUser($search, $this->role);
        }

        if(!empty($users))
        {
            foreach ($users as $user)
            {
                $edit =  url('/admin-access/edit-user/'.$user->id);
                $delete =  url('/admin-access/delete-user/'.$user->id);

                $nestedData['id'] = $user->id;
                $nestedData['name'] = $user->name;
                $nestedData['email'] = $user->email;
                $nestedData['role'] = $user->role;

                $nestedData['action_btn'] = "
                    <button onclick='master_edit(\"".$edit."\")' type='button' class='btn btn-info mr-1 mb-1'><i class='ft-edit'></i></button>
                    <button onclick='master_delete(\"".$delete."\", \"".'user'."\")' type='button' class='btn btn-danger mr-1 mb-1'><i class='ft-trash-2'></i></button>
                ";
                
                $data[] = $nestedData;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function getAddUser()
    {
    	return view('admin/add-user', ['user' => Auth::user(), 'page_title' => $this->page_title_user]);
    }

    public function postAddUser()
    {
    	$requested = request()->validate([
    		'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'role' => 'required'
    	]);

    	$result = $this->userModel->postAddUser($requested);
    	
    	return redirect()->route('getEditUser', ['id' => $result[0]])->with(['done' => $result[1]] );
    }

    public function getEditUser($id)
    {
    	$user = $this->userModel->getOneUser($id, $this->role);

    	if($user == null)
    	{
    		return redirect()->route('getUser');
    	}

    	return view('admin/edit-user', ['user' => Auth::user(), 'user' => $user, 'page_title' => $this->page_title_user]);
    }

    public function postEditUser($id)
    {
    	$user = $this->userModel->getOneUser($id, $this->role);

    	if($user == null)
    	{
    		return redirect()->route('getUser');
    	}

    	$requested = request()->validate([
    		'name' => 'required',
            'email' => 'required',
            'password' => '',
            'role' => 'required'
    	]);

    	$result = $this->userModel->postEditUser($requested, $id);

    	return redirect()->route('getEditUser', ['id' => $id])->with(['done' => $result] );
    }

    public function postDeleteUser($id)
    {
    	$user = $this->userModel->getOneUser($id, $this->role);

    	if($user == null)
    	{
    		return redirect()->route('getUser');
    	}

    	$result = $this->userModel->postDeleteUser($id);

    	return json_encode($result);
    }
}
