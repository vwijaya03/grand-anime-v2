<?php

namespace App\Http\Controllers;

use App\Http\Controllers\AuthorizedRolesController;
use Illuminate\Http\Request;
use App\VideoDescriptionModel;
use Auth, Hash, DB, Log;

class AnimeController extends Controller
{
    public function __construct(VideoDescriptionModel $videoDescriptionModel)
    {
        $this->videoDescriptionModel = $videoDescriptionModel;
        DB::enableQueryLog();
    }

    public function getAnime()
    {
        return view('admin/anime', ['user' => Auth::user()]);
    }

    public function postAjaxAnime(Request $request)
    {
        $data = array();

        $columns = array( 
            0 => 'id', 
            1 => 'sub_category',
            2 => 'title',
            3 => 'released',
            4 => 'status',
            5 => 'id'
        );
  
        $totalData = $this->videoDescriptionModel->countAllActiveAnime();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value'); 

        if(empty($search))
        {            
            $animes = $this->videoDescriptionModel->getAnime($start, $limit, $order, $dir);
        }
        else 
        {
            $animes = $this->videoDescriptionModel->getFilteredAnime($search, $start, $limit, $order, $dir);
            $totalFiltered = $this->videoDescriptionModel->countAllFilteredActiveAnime($search);
        }

        if(!empty($animes))
        {
            foreach ($animes as $anime)
            {
                $edit =  url('/admin-access/edit-anime/'.$anime->id);
                $delete =  url('/admin-access/delete-anime/'.$anime->id);

                $nestedData['id'] = $anime->id;
                $nestedData['sub_category'] = $anime->sub_category;
                $nestedData['title'] = $anime->title;
                $nestedData['released'] = $anime->released;
                $nestedData['status'] = $anime->status;

                $nestedData['action_btn'] = "
                    <button onclick='master_edit(\"".$edit."\")' type='button' class='btn btn-info mr-1 mb-1'><i class='ft-edit'></i></button>
                    <button onclick='master_delete(\"".$delete."\", \"".'anime'."\")' type='button' class='btn btn-danger mr-1 mb-1'><i class='ft-trash-2'></i></button>
                    <a href='".url('/admin-access/anime-episode-with-anime-id/'.$anime->id)."' class='btn btn-success mr-1 mb-1'>EP</a>
                    <a href='".url('/admin-access/anime-genre-with-anime-id/'.$anime->id)."' class='btn btn-primary mr-1 mb-1'>Genre</a>
                ";
                
                $data[] = $nestedData;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function getAddAnime()
    {
    	return view('admin/add-anime', ['user' => Auth::user()]);
    }

    public function postAddAnime()
    {
    	$requested = request()->validate([
    		'sub_category' => 'required',
            'img' => 'required',
            'title' => 'required',
            'description' => 'required',
            'released' => 'required',
            'status' => 'required'
    	]);

    	$result = $this->videoDescriptionModel->postAddAnime($requested);
    	
    	return redirect()->route('getEditAnime', ['id' => $result[0]])->with(['done' => $result[1]] );
    }

    public function getEditAnime($id)
    {
    	$anime = $this->videoDescriptionModel->getOneAnime($id);

    	if($anime == null)
    	{
    		return redirect()->route('getAnime');
    	}

    	return view('admin/edit-anime', ['user' => Auth::user(), 'anime' => $anime]);
    }

    public function postEditAnime($id)
    {
    	$anime = $this->videoDescriptionModel->getOneAnime($id);

    	if($anime == null)
    	{
    		return redirect()->route('getAnime');
    	}

    	$requested = request()->validate([
    		'sub_category' => 'required',
    		'img' => 'required',
    		'title' => 'required',
    		'description' => 'required',
    		'released' => 'required',
    		'status' => 'required'
    	]);

    	$result = $this->videoDescriptionModel->postEditAnime($requested, $id);

    	return redirect()->route('getEditAnime', ['id' => $id])->with(['done' => $result] );
    }

    public function postDeleteAnime($id)
    {
    	$anime = $this->videoDescriptionModel->getOneAnime($id);

    	if($anime == null)
    	{
    		return redirect()->route('getAnime');
    	}

    	$result = $this->videoDescriptionModel->postDeleteAnime($id);

    	return json_encode($result);
    }
}
