<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\VideoDescriptionModel;
use App\VideoEpisodeModel;
use Auth, Hash, DB, Log;

class AnimeEpisodeController extends Controller
{
	public function __construct(VideoDescriptionModel $videoDescriptionModel, VideoEpisodeModel $videoEpisodeModel)
    {
        $this->videoDescriptionModel = $videoDescriptionModel;
        $this->videoEpisodeModel = $videoEpisodeModel;
        DB::enableQueryLog();
    }

    public function getAnimeEpisode($anime_id)
    {
    	$anime = $this->videoDescriptionModel->getOneAnime($anime_id);

    	if($anime == null)
    	{
    		return redirect()->route('getAnime');
    	}
    	
        return view('admin/anime-episode', ['user' => Auth::user(), 'anime_id' => $anime_id, 'anime' => $anime]);
    }

    public function postAjaxAnimeEpisode(Request $request, $anime_id)
    {
        $data = array();

        $columns = array( 
            0 => 'id', 
            1 => 'title',
            2 => 'id'
        );
  
        $totalData = $this->videoEpisodeModel->countAllActiveAnimeEpisode($anime_id);
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value'); 

        if(empty($search))
        {            
            $anime_episodes = $this->videoEpisodeModel->getAnimeEpisode($start, $limit, $order, $dir, $anime_id);
        }
        else 
        {
            $anime_episodes = $this->videoEpisodeModel->getFilteredAnimeEpisode($search, $start, $limit, $order, $dir, $anime_id);
            $totalFiltered = $this->videoEpisodeModel->countAllFilteredActiveAnimeEpisode($search, $anime_id);
        }

        if(!empty($anime_episodes))
        {
            foreach ($anime_episodes as $anime_episode)
            {
                $edit =  url('/admin-access/edit-anime-episode/'.$anime_episode->id.'/with-anime-id/'.$anime_id);
                $delete =  url('/admin-access/delete-anime-episode/'.$anime_episode->id.'/with-anime-id/'.$anime_id);
                $callback_delete = url('/admin-access/anime-episode-with-anime-id/'.$anime_id);

                $nestedData['id'] = $anime_episode->id;
                $nestedData['title'] = $anime_episode->title;

                $nestedData['action_btn'] = "
                    <button onclick='master_edit(\"".$edit."\")' type='button' class='btn btn-info mr-1 mb-1'><i class='ft-edit'></i></button>
                    <button onclick='master_delete(\"".$delete."\", \"".$callback_delete."\")' type='button' class='btn btn-danger mr-1 mb-1'><i class='ft-trash-2'></i></button>
                    <a href='".url('/admin-access/anime-streaming-with-anime-episode-id/'.$anime_episode->id)."' class='btn btn-success mr-1 mb-1'>Streaming</a>
                    <a href='".url('/admin-access/anime-download-with-anime-episode-id/'.$anime_episode->id)."' class='btn btn-success mr-1 mb-1'>Download</a>
                ";
                
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function getAddAnimeEpisode($anime_id)
    {
    	$anime = $this->videoDescriptionModel->getOneAnime($anime_id);

    	if($anime == null)
    	{
    		return redirect()->route('getAnime');
    	}

    	return view('admin/add-anime-episode', ['user' => Auth::user(), 'anime_id' => $anime_id, 'anime' => $anime]);
    }

    public function postAddAnimeEpisode($anime_id)
    {
    	$requested = request()->validate([
    		'title' => 'required',
    		'slug' => 'required'
    	]);

    	$anime = $this->videoDescriptionModel->getOneAnime($anime_id);

    	if($anime == null)
    	{
    		return redirect()->route('getAnime');
    	}

    	$result = $this->videoEpisodeModel->postAddAnimeEpisode($requested, $anime_id);
    	
    	return redirect()->route('getEditAnimeEpisode', ['id' => $result[0], 'anime_id' => $anime_id])->with(['done' => $result[1]] );
    }

    public function getEditAnimeEpisode($id, $anime_id)
    {
    	$anime_episode = $this->videoEpisodeModel->getOneAnimeEpisode($id, $anime_id);

    	if($anime_episode == null)
    	{
    		return redirect()->route('getAnime');
    	}

    	$anime = $this->videoDescriptionModel->getOneAnime($anime_id);

    	if($anime == null)
    	{
    		return redirect()->route('getAnime');
    	}

    	return view('admin/edit-anime-episode', ['user' => Auth::user(), 'anime_episode' => $anime_episode, 'anime_id' => $anime_id, 'anime' => $anime]);
    }

    public function postEditAnimeEpisode($id, $anime_id)
    {
    	$anime_episode = $this->videoEpisodeModel->getOneAnimeEpisode($id, $anime_id);

    	if($anime_episode == null)
    	{
    		return redirect()->route('getAnime');
    	}

    	$requested = request()->validate([
    		'title' => 'required',
    		'slug' => 'required'
    	]);

    	$result = $this->videoEpisodeModel->postEditAnimeEpisode($requested, $id, $anime_id);

    	return redirect()->route('getEditAnimeEpisode', ['id' => $id, 'anime_id' => $anime_id])->with(['done' => $result] );
    }

    public function postDeleteAnimeEpisode($id, $anime_id)
    {
    	$anime_episode = $this->videoEpisodeModel->getOneAnimeEpisode($id, $anime_id);

    	if($anime_episode == null)
    	{
    		return redirect()->route('getAnime');
    	}

    	$result = $this->videoEpisodeModel->postDeleteAnimeEpisode($id, $anime_id);

    	return json_encode($result);
    }
}
