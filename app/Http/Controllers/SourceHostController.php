<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SourceHostModel;
use Auth, Hash, DB, Log;

class SourceHostController extends Controller
{
    private $sourceHostModel;

    public function __construct(SourceHostModel $sourceHostModel)
    {
        $this->sourceHostModel = $sourceHostModel;
    }

    public function getSourceHost()
    {
        return view('admin/source-host', ['user' => Auth::user()]);
    }

    public function postAjaxGetSourceHost(Request $request)
    {
        $data = array();

        $columns = array( 
            0 => 'id', 
            1 => 'protocol',
            2 => 'url',
            3 => 'additional_url',
            4 => 'recent_page',
            5 => 'episode_start_page',
            6 => 'episode_end_page',
            7 => 'tipe',
            8 => 'id'
        );
  
        $totalData = $this->sourceHostModel->countAllActiveSourceHost();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value'); 

        if(empty($search))
        {            
            $source_hosts = $this->sourceHostModel->getSourceHost($start, $limit, $order, $dir);
        }
        else 
        {
            $source_hosts = $this->sourceHostModel->getFilteredSourceHost($search, $start, $limit, $order, $dir);
            $totalFiltered = $this->sourceHostModel->countAllFilteredActiveSourceHost($search);
        }

        if(!empty($source_hosts))
        {
            foreach ($source_hosts as $source_host)
            {
                $edit =  url('/admin-access/edit-source-host/'.$source_host->id);
                $delete =  url('/admin-access/delete-source-host/'.$source_host->id);
                $access_url_run_garuk = url('/admin-access/'.$source_host->tipe);

                $nestedData['id'] = $source_host->id;
                $nestedData['protocol'] = $source_host->protocol;
                $nestedData['url'] = $source_host->url;
                $nestedData['additional_url'] = $source_host->additional_url;
                $nestedData['recent_page'] = $source_host->recent_page;
                $nestedData['episode_start_page'] = $source_host->episode_start_page;
                $nestedData['episode_end_page'] = $source_host->episode_end_page;
                $nestedData['tipe'] = $source_host->tipe;

                $nestedData['action_btn'] = "
                	<button onclick='run_garuk(\"".$access_url_run_garuk."\", \"".$source_host->protocol."\", \"".$source_host->url."\", \"".$source_host->additional_url."\", \"".$source_host->recent_page."\", \"".$source_host->episode_start_page."\", \"".$source_host->episode_end_page."\", \"".$source_host->tipe."\")' type='button' class='btn btn-secondary mr-1 mb-1'><i class='ft-play'></i></button>

                    <button onclick='master_edit(\"".$edit."\")' type='button' class='btn btn-info mr-1 mb-1'><i class='ft-edit'></i></button>
                    <button onclick='master_delete(\"".$delete."\", \"".'source-host'."\")' type='button' class='btn btn-danger mr-1 mb-1'><i class='ft-trash-2'></i></button>
                ";
                
                $data[] = $nestedData;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function getAddSourceHost()
    {
    	return view('admin/add-source-host', ['user' => Auth::user()]);
    }

    public function postAddSourceHost()
    {
    	$requested = request()->validate([
    		'protocol' => 'required',
    		'url' => 'required',
    		'additional_url' => '',
    		'recent_page' => '',
    		'episode_start_page' => '',
    		'episode_end_page' => '',
    		'tipe' => 'required'
    	]);

    	$result = $this->sourceHostModel->postAddSourceHost($requested);
    	
    	return redirect()->route('getEditSourceHost', ['id' => $result[0]])->with(['done' => $result[1]] );
    }

    public function getEditSourceHost($id)
    {
    	$source_host = $this->sourceHostModel->getOneSourceHost($id);

    	if($source_host == null)
    	{
    		return redirect()->route('getSourceHost');
    	}

    	return view('admin/edit-source-host', ['user' => Auth::user(), 'source_host' => $source_host]);
    }

    public function postEditSourceHost($id)
    {
    	$source_host = $this->sourceHostModel->getOneSourceHost($id);

    	if($source_host == null)
    	{
    		return redirect()->route('getSourceHost');
    	}

    	$requested = request()->validate([
    		'protocol' => 'required',
    		'url' => 'required',
    		'additional_url' => '',
    		'recent_page' => '',
    		'episode_start_page' => '',
    		'episode_end_page' => '',
    		'tipe' => 'required'
    	]);

    	$result = $this->sourceHostModel->postEditSourceHost($requested, $id);

    	return redirect()->route('getEditSourceHost', ['id' => $id])->with(['done' => $result] );
    }

    public function postDeleteSourceHost($id)
    {
    	$source_host = $this->sourceHostModel->getOneSourceHost($id);

    	if($source_host == null)
    	{
    		return redirect()->route('getSourceHost');
    	}

    	$result = $this->sourceHostModel->postDeleteSourceHost($id);

    	return json_encode($result);
    }
}
