<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth, Hash, DB, Log;
use App\KategoriModel;
use App\VideoDescriptionModel;
use App\VideoEpisodeModel;
use App\ConnectVideoAndGenreModel;
use App\StreamingUrlModel;
use App\DownloadUrlModel;

class FrontEndController extends Controller
{
	public function __construct(ConnectVideoAndGenreModel $connectVideoAndGenreModel)
    {
        $this->connectVideoAndGenreModel = $connectVideoAndGenreModel;
    }

    public function getFEHome()
    {
    	$genres = $this->connectVideoAndGenreModel->getFEGenre();
    	
    	$anime_episodes = VideoEpisodeModel::select('GA_Video.title as anime_title', 'GA_Video.img as anime_img', 'GA_Video.slug as anime_slug', 'GA_VideoEpisode.slug as anime_episode_slug', 'GA_VideoEpisode.title as anime_episode_title')
    	->join('GA_Video', 'GA_Video.id', '=', 'GA_VideoEpisode.video_description_id')
    	->where('GA_Video.delete', 0)
    	->where('GA_VideoEpisode.delete', 0)
    	->orderBy('GA_VideoEpisode.created_at', 'desc')
    	->limit(20)
    	->get();

    	$ongoing_animes = VideoDescriptionModel::select('GA_Video.title', 'GA_Video.slug', 'GA_Video.img')
    	->where('status', 'Ongoing')
    	->where('delete', 0)
    	->orderBy('created_at', 'desc')
    	->limit(12)
    	->get();

    	return view('fe/home', ['genres' => $genres, 'anime_episodes' => $anime_episodes, 'ongoing_animes' => $ongoing_animes]);
    }

    public function getFERecentReleaseAnime()
    {
    	$anime_episodes = VideoEpisodeModel::select('GA_Video.title as anime_title', 'GA_Video.img as anime_img', 'GA_Video.slug as anime_slug', 'GA_VideoEpisode.slug as anime_episode_slug', 'GA_VideoEpisode.title as anime_episode_title')
    	->join('GA_Video', 'GA_Video.id', '=', 'GA_VideoEpisode.video_description_id')
    	->where('GA_Video.delete', 0)
    	->where('GA_VideoEpisode.delete', 0)
    	->orderBy('GA_VideoEpisode.created_at', 'desc')
    	->paginate(20);

    	return view('fe/recent-release', ['anime_episodes' => $anime_episodes]);
    }

    public function getFEOngoingAnime()
    {
    	$ongoing_animes = VideoDescriptionModel::select('GA_Video.title', 'GA_Video.slug', 'GA_Video.img')
    	->where('status', 'Ongoing')
    	->where('delete', 0)
    	->orderBy('created_at', 'desc')
    	->paginate(20);

    	return view('fe/ongoing', ['ongoing_animes' => $ongoing_animes]);
    }

    public function getFESearchAnime()
    {
    	$requested = request()->validate([
            'q' => ''
    	]);

    	$animes = VideoDescriptionModel::select('id', 'title', 'slug', 'img')
        ->where('title', 'like', '%'.$requested['q'].'%')
        ->where('delete', 0)
        ->orderBy('title','asc')
        ->paginate(20);

        return view('fe/search-anime', ['animes' => $animes]);
    }

    public function getFENewSeasonAnime()
    {
    	$animes = VideoDescriptionModel::select('GA_Video.title', 'GA_Video.slug', 'GA_Video.img')
    	->where('released', '>=', date('Y'))
    	->where('delete', 0)
    	->orderBy('created_at', 'desc')
    	->paginate(20);

    	return view('fe/new-season-anime', ['animes' => $animes]);
    }

    public function getFEMoviesAnime()
    {
    	$animes = VideoDescriptionModel::select('GA_Video.title', 'GA_Video.slug', 'GA_Video.img')
    	->where('sub_category_slug', 'movies')
    	->where('delete', 0)
    	->orderBy('created_at', 'desc')
    	->paginate(20);

    	return view('fe/movies-anime', ['animes' => $animes]);
    }

    public function getFEGenreAnime($slug)
    {	
    	if($slug == '' || $slug == null)
    	{
    		return redirect()->route('getFEHome');
    	}

    	$animes = VideoDescriptionModel::select('GA_Video.title', 'GA_Video.slug', 'GA_Video.img')
    	->join('GA_ConnectionVideoDescAndGenre', 'GA_ConnectionVideoDescAndGenre.video_description_id', '=', 'GA_Video.id')
    	->where('GA_ConnectionVideoDescAndGenre.genre_slug', $slug)
    	->where('GA_ConnectionVideoDescAndGenre.delete', 0)
    	->where('GA_Video.delete', 0)
    	->orderBy('GA_Video.created_at', 'desc')
    	->paginate(20);

    	return view('fe/genre-anime', ['animes' => $animes]);
    }

    public function getFEAnime($slug)
    {
    	if($slug == '' || $slug == null)
    	{
    		return redirect()->route('getFEHome');
    	}

    	$anime = VideoDescriptionModel::select('GA_Video.id', 'GA_Video.title', 'GA_Video.slug', 'GA_Video.img', 'GA_Video.sub_category', 'GA_Video.sub_category_slug', 'GA_Video.description', 'GA_Video.released', 'GA_Video.status')
    	->where('GA_Video.slug', $slug)
    	->where('GA_Video.delete', 0)
    	->first();

    	if($anime == '' || $anime == null)
    	{
    		return redirect()->route('getFEHome');
    	}

    	$anime_episodes = VideoEpisodeModel::select('slug', 'title')
    	->where('video_description_id', $anime->id)
    	->where('delete', 0)
    	->orderByRaw('LENGTH(title) desc, title desc')
    	->paginate(15);

    	$genres = ConnectVideoAndGenreModel::select('genre_slug', 'genre_text')
    	->where('video_description_id', $anime->id)
    	->where('delete', 0)
    	->get();

    	return view('fe/anime', ['anime' => $anime, 'anime_episodes' => $anime_episodes, 'genres' => $genres]);
    }

    public function getFESubCategoryAnime($slug)
    {
    	if($slug == '' || $slug == null)
    	{
    		return redirect()->route('getFEHome');
    	}

    	$animes = VideoDescriptionModel::select('GA_Video.title', 'GA_Video.slug', 'GA_Video.img')
    	->where('GA_Video.sub_category_slug', $slug)
    	->where('GA_Video.delete', 0)
    	->orderBy('GA_Video.created_at', 'desc')
    	->paginate(20);

    	return view('fe/sub-category', ['animes' => $animes]);
    }

    public function getFEAnimeEpisode($slug)
    {
    	if($slug == '' || $slug == null)
    	{
    		return redirect()->route('getFEHome');
    	}

    	$anime_episode = VideoEpisodeModel::select('GA_VideoEpisode.id', 'GA_VideoEpisode.slug', 'GA_VideoEpisode.title', 'GA_Video.img')
    	->join('GA_Video', 'GA_Video.id', '=', 'GA_VideoEpisode.video_description_id')
    	->where('GA_VideoEpisode.slug', $slug)
    	->where('GA_Video.delete', 0)
    	->where('GA_VideoEpisode.delete', 0)
    	->first();

    	if($anime_episode == '' || $anime_episode == null)
    	{
    		return redirect()->route('getFEHome');
    	}

    	$streamings = StreamingUrlModel::select('button_name', 'url')
    	->where('video_episode_id', $anime_episode->id)
    	->where('delete', 0)
    	->get();

    	$download_links = DownloadUrlModel::select('button_name', 'url')
    	->where('video_episode_id', $anime_episode->id)
    	->where('delete', 0)
    	->get();

    	return view('fe/streaming-and-download', ['anime_episode' => $anime_episode, 'streamings' => $streamings, 'count_streaming_links' => count($streamings), 'download_links' => $download_links, 'count_download_links' => count($download_links)]);
    }

    public function getFEAnimeList()
    {
    	$animes = VideoDescriptionModel::select('GA_Video.title', 'GA_Video.slug', 'GA_Video.img')
    	->where('delete', 0)
    	->orderBy('title', 'asc')
    	->paginate(20);

    	return view('fe/anime-list', ['animes' => $animes, 'alphabet' => '#']);
    }

    public function getFEAnimeListByAlphabet($alphabet)
    {
    	$animes = VideoDescriptionModel::select('GA_Video.title', 'GA_Video.slug', 'GA_Video.img')
    	->where('title', 'like', $alphabet.'%')
    	->where('delete', 0)
    	->orderBy('title', 'asc')
    	->paginate(20);

    	return view('fe/anime-list', ['animes' => $animes, 'alphabet' => $alphabet]);
    }
}
