<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Mail\ResetPasswordMail;
use App\UserModel;
use Auth, Hash, DB, Log, Carbon;

class AdminLoginController extends Controller
{
    public function __construct()
    {
        
    }

    public function getAdminLogin(Request $request)
    {
        if(Auth::check())
        {
            return redirect()->route('getAdminDashboard');
        }
        
        return view('admin/login');
    }

    public function postAdminLogin()
    {
        $requested = request()->validate([
    		'email' => 'required',
    		'password' => 'required'
    	]);

        if(Auth::attempt(['email' => $requested['email'], 'password' => $requested['password'], 'delete' => 0]))
        {
            return redirect()->route('getAdminDashboard');
        }
        else if(!Auth::attempt(['email' => $requested['email'], 'password' => $requested['password'], 'delete' => 0]))
        {
            return redirect()->route('getAdminLogin')->with(['err' => 'E-mail atau password salah.']);
        }
        else
        {
            return redirect()->route('getAdminLogin')->with(['err' => 'Akses ditolak.']);
        }
    }

    public function getAdminLogout()
    {
        Auth::logout();
        return redirect()->route('getAdminLogin');
    }

    public function postChangePassword(Request $request)
    {
        if(Auth::user()->delete == "1" || Auth::user()->delete == 1)
        {   
            return redirect()->route('getLogin')->with(['err' => 'Maaf, anda tidak di izinkan untuk mengakses aplikasi ini.']);
        }

        $current_password = $request->get('old_password');
        $new_password = $request->get('new_password');
        $email = Auth::user()->email;

        if(Hash::check($current_password, Auth::user()->password))
        {
            Log::info('masuk if');
            UserModel::where('email', $email)->where('delete', 0)
            ->update([
                'password' => Hash::make($new_password)
            ]);
            return redirect()->route('getChangePassword')->with(['done' => 'Password berhasil di ubah.']);
        }
        else
        {
            return redirect()->route('getChangePassword')->with(['err' => 'Password gagal di ubah.']);
        }
    }

    public function getChangePassword()
    {
        return view('admin/change-password', ['user' => Auth::user()]);
    }

    public function getResetPassword()
    {
        return view('admin/reset-password');
    }

    public function postResetPassword(Request $request)
    {
        $faker = \Faker\Factory::create();
        $reset_password = $faker->randomNumber($nbDigits = 6, $strict = false);
        $msg = '';
        $status_json = '';

        $email = $request->get('email');
        Log::info('new password: '.$reset_password);

        $result = UserModel::select('id', 'email')->where('email', $email)->where('delete', 0)->first();

        if($result != null)
        {
            Log::info('reset email: '.$result->email);

            UserModel::where('email', $result->email)->where('delete', 0)
            ->update([
                'password' => Hash::make($reset_password)
            ]);

            $to_email = $result->email;
            // $to_email = 'viko_wijaya@yahoo.co.id';

            Mail::to($to_email)->send(new ResetPasswordMail($reset_password));
            $msg = 'Password Baru Anda Berhasil Di Kirim Ke Email.';

            return redirect()->route('getResetPassword')->with(['done' => $msg]);
        }
        else 
        {
            return redirect()->route('getResetPassword')->with(['err' => 'Data user tidak ditemukan.']);
        }
    }
}