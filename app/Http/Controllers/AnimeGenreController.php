<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\VideoDescriptionModel;
use App\ConnectVideoAndGenreModel;
use Auth, Hash, DB, Log;

class AnimeGenreController extends Controller
{
    public function __construct(VideoDescriptionModel $videoDescriptionModel, ConnectVideoAndGenreModel $connectVideoAndGenreModel)
    {
        $this->videoDescriptionModel = $videoDescriptionModel;
        $this->connectVideoAndGenreModel = $connectVideoAndGenreModel;
        DB::enableQueryLog();
    }

    public function getAnimeGenre($anime_id)
    {
    	$anime = $this->videoDescriptionModel->getOneAnime($anime_id);

    	if($anime == null)
    	{
    		return redirect()->route('getAnime');
    	}
    	
        return view('admin/anime-genre', ['user' => Auth::user(), 'anime_id' => $anime_id, 'anime' => $anime]);
    }

    public function postAjaxAnimeGenre(Request $request, $anime_id)
    {
        $data = array();

        $columns = array( 
            0 => 'id', 
            1 => 'genre_text',
            2 => 'id'
        );
  
        $totalData = $this->connectVideoAndGenreModel->countAllActiveAnimeGenre($anime_id);
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value'); 

        if(empty($search))
        {            
            $anime_genres = $this->connectVideoAndGenreModel->getAnimeGenre($start, $limit, $order, $dir, $anime_id);
        }
        else 
        {
            $anime_genres = $this->connectVideoAndGenreModel->getFilteredAnimeGenre($search, $start, $limit, $order, $dir, $anime_id);
            $totalFiltered = $this->connectVideoAndGenreModel->countAllFilteredActiveAnimeGenre($search, $anime_id);
        }

        if(!empty($anime_genres))
        {
            foreach ($anime_genres as $anime_genre)
            {
                $edit =  url('/admin-access/edit-anime-genre/'.$anime_genre->id.'/with-anime-id/'.$anime_id);
                $delete =  url('/admin-access/delete-anime-genre/'.$anime_genre->id.'/with-anime-id/'.$anime_id);
                $callback_delete = url('/admin-access/anime-genre-with-anime-id/'.$anime_id);

                $nestedData['id'] = $anime_genre->id;
                $nestedData['genre_text'] = $anime_genre->genre_text;

                $nestedData['action_btn'] = "
                    <button onclick='master_edit(\"".$edit."\")' type='button' class='btn btn-info mr-1 mb-1'><i class='ft-edit'></i></button>
                    <button onclick='master_delete(\"".$delete."\", \"".$callback_delete."\")' type='button' class='btn btn-danger mr-1 mb-1'><i class='ft-trash-2'></i></button>
                ";
                
                $data[] = $nestedData;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        return json_encode($json_data);
    }

    public function getAddAnimeGenre($anime_id)
    {
    	$anime = $this->videoDescriptionModel->getOneAnime($anime_id);

    	if($anime == null)
    	{
    		return redirect()->route('getAnime');
    	}

    	return view('admin/add-anime-genre', ['user' => Auth::user(), 'anime_id' => $anime_id, 'anime' => $anime]);
    }

    public function postAddAnimeGenre($anime_id)
    {
    	$requested = request()->validate([
    		'genre_text' => 'required',
    		'genre_slug' => 'required'
    	]);

    	$anime = $this->videoDescriptionModel->getOneAnime($anime_id);

    	if($anime == null)
    	{
    		return redirect()->route('getAnime');
    	}

    	$result = $this->connectVideoAndGenreModel->postAddAnimeGenre($requested, $anime_id);
    	
    	return redirect()->route('getEditAnimeGenre', ['id' => $result[0], 'anime_id' => $anime_id])->with(['done' => $result[1]] );
    }

    public function getEditAnimeGenre($id, $anime_id)
    {
    	$anime_genre = $this->connectVideoAndGenreModel->getOneAnimeGenre($id, $anime_id);

    	if($anime_genre == null)
    	{
    		return redirect()->route('getAnime');
    	}

    	$anime = $this->videoDescriptionModel->getOneAnime($anime_id);

    	if($anime == null)
    	{
    		return redirect()->route('getAnime');
    	}

    	return view('admin/edit-anime-genre', ['user' => Auth::user(), 'anime_genre' => $anime_genre, 'anime_id' => $anime_id, 'anime' => $anime]);
    }

    public function postEditAnimeGenre($id, $anime_id)
    {
    	$anime_genre = $this->connectVideoAndGenreModel->getOneAnimeGenre($id, $anime_id);

    	if($anime_genre == null)
    	{
    		return redirect()->route('getAnime');
    	}

    	$requested = request()->validate([
    		'genre_text' => 'required',
    		'genre_slug' => 'required'
    	]);

    	$result = $this->connectVideoAndGenreModel->postEditAnimeGenre($requested, $id, $anime_id);

    	return redirect()->route('getEditAnimeGenre', ['id' => $id, 'anime_id' => $anime_id])->with(['done' => $result] );
    }

    public function postDeleteAnimeGenre($id, $anime_id)
    {
    	$anime_genre = $this->connectVideoAndGenreModel->getOneAnimeGenre($id, $anime_id);

    	if($anime_genre == null)
    	{
    		return redirect()->route('getAnime');
    	}

    	$result = $this->connectVideoAndGenreModel->postDeleteAnimeGenre($id, $anime_id);

    	return json_encode($result);
    }
}
