<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth, Hash, DB, Log;

class StreamingUrlModel extends Model
{
    protected $table = 'GA_StreamingUrl';
	protected $primaryKey = 'id';
    protected $fillable = ['video_episode_id', 'button_name', 'url', 'delete'];

    private $success_update_msg = 'Data berhasil di ubah.';
    private $success_add_msg = 'Data berhasil di tambahkan.';
    private $success_delete_msg = 'Data berhasil di hapus.';

    public function countAllActiveAnimeStreaming($anime_episode_id)
    {
        $count_anime_streaming = $this->select('id')
        ->where('video_episode_id', $anime_episode_id)
        ->where('delete', 0)
        ->count();

        return $count_anime_streaming;
    }

    public function countAllFilteredActiveAnimeStreaming($search, $anime_episode_id)
    {
        $count_anime_streaming = $this->select('id')
        ->where(function ($q) use($search) {
            $q->where('id', 'like', '%'.$search.'%');
            $q->orWhere('button_name', 'like', '%'.$search.'%');
            $q->orWhere('url', 'like', '%'.$search.'%');
        })
        ->where('video_episode_id', $anime_episode_id)
        ->where('delete', 0)
        ->count();

        return $count_anime_streaming;
    }

    public function getAnimeStreaming($start, $limit, $order, $dir, $anime_episode_id)
    {
    	$anime_streaming = $this->select('id', 'button_name', 'url')
    	->where('video_episode_id', $anime_episode_id)
        ->where('delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

    	return $anime_streaming;
    }

    public function getFilteredAnimeStreaming($search, $start, $limit, $order, $dir, $anime_episode_id)
    {
        $anime_streaming = $this->select('id', 'button_name', 'url')
        ->where(function ($q) use($search) {
            $q->where('id', 'like', '%'.$search.'%');
            $q->orWhere('button_name', 'like', '%'.$search.'%');
            $q->orWhere('url', 'like', '%'.$search.'%');
        })
        ->where('video_episode_id', $anime_episode_id)
        ->where('delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $anime_streaming;
    }

    public function getOneAnimeStreaming($id, $anime_episode_id)
    {
    	$anime_streaming = $this->select('id', 'button_name', 'url')
    	->where('id', $id)
    	->where('video_episode_id', $anime_episode_id)
    	->where('delete', 0)
    	->first();

    	return $anime_streaming;
    }

    public function postAddAnimeStreaming($param, $anime_episode_id)
    {
    	$result = [];

    	$final = DB::transaction(function () use($param, $result, $anime_episode_id) {
		    $data = $this->create([
		    	'video_episode_id' => $anime_episode_id,
                'button_name' => $param['button_name'],
                'url' => $param['url'],
		    	'delete' => 0
		    ]);

		   	$result[0] = $data->id;
		   	$result[1] = $this->success_add_msg;

		    return $result;
		});

		return $final;
    }

    public function postEditAnimeStreaming($param, $id, $anime_episode_id)
    {
    	DB::transaction(function () use($param, $id, $anime_episode_id) {
		    $this->where('id', $id)->where('delete', 0)->update([
		    	'video_episode_id' => $anime_episode_id,
                'button_name' => $param['button_name'],
                'url' => $param['url']
		    ]);
		});

		return $this->success_update_msg;
    }

    public function postDeleteAnimeStreaming($id, $anime_episode_id)
    {
    	$result = [];

    	DB::transaction(function () use($id, $anime_episode_id) {
		    $this->where('id', $id)->where('video_episode_id', $anime_episode_id)->where('delete', 0)->update([
		    	'delete' => 1
		    ]);
		});

    	$result['message'] = $this->success_update_msg;

		return $result;
    }
}
