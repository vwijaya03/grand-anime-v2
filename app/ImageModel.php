<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageModel extends Model
{
    protected $table = 'GA_ImageNameAndPath';
	protected $primaryKey = 'id';
    protected $fillable = ['user_id', 'name', 'img_path', 'delete'];
}
