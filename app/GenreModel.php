<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GenreModel extends Model
{
    protected $table = 'GA_Genre';
	protected $primaryKey = 'id';
    protected $fillable = ['slug', 'name', 'delete'];
}
