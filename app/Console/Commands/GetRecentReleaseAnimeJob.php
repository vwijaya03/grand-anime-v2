<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Auth, Hash, DB, Log, Carbon;
use RuntimeException;
use Goutte\Client;
use App\KategoriModel;
use App\VideoDescriptionModel;
use App\VideoEpisodeModel;
use App\ConnectVideoAndGenreModel;
use App\StreamingUrlModel;
use App\DownloadUrlModel;

class GetRecentReleaseAnimeJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:recent_release_anime_job';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Recent Release Anime';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $target_url = 'https://gogoanimes.co/page-recent-release.html?page=1';
        $base_url = 'https://gogoanimes.co';

        $plot_summary = "Plot Summary: ";
        $plot_summary_len = strlen($plot_summary);

        $released = "Released: ";
        $released_len = strlen($released);

        $ongoing_status = "Status: ";
        $ongoing_status_len = strlen($ongoing_status);

        $remove_category_string_for_slug = strlen("/category/");
        $remove_category_string_for_slug = strlen("/category/");
        $remove_sub_category_string_for_slug = strlen("/sub-category/");
        $remove_choose_this_server_string = strlen("Choose this server");
        $minus_remove_choose_this_server_string = $remove_choose_this_server_string * -1;

        $result_anime_detail = [];
        $result_anime_episode = [];
        $result_anime_episode_streaming = [];
        $result_anime_episode_download = [];
        $result_all_recent_release = [];

        $recent_release_client = new Client();

        $crawler_recent_release = $recent_release_client->request('GET', $target_url);

        $data_recent_releases = $crawler_recent_release->filter('.items .name a')->each(function ($node, $i) use($crawler_recent_release, $recent_release_client, $remove_sub_category_string_for_slug, $remove_category_string_for_slug, $minus_remove_choose_this_server_string, $base_url, $plot_summary_len, $released_len, $ongoing_status_len, $result_anime_detail, $result_anime_episode, $result_all_recent_release, $result_anime_episode_streaming, $result_anime_episode_download) {
        
            $anime_episode_detail_link = $crawler_recent_release->selectLink(trim($node->text()))->link();
            $crawler_anime_episode_detail = $recent_release_client->click($anime_episode_detail_link);

            // Anime Info
            $anime_info = $crawler_anime_episode_detail->filter('.anime-info a')->attr('title');
            Log::info('isi anime info: '.$anime_info);
            Log::info(' ');

            // Anime Info Slug
            $anime_info_slug = substr($crawler_anime_episode_detail->filter('.anime-info a')->attr('href'), $remove_category_string_for_slug);
            Log::info('isi anime info slug: '.$anime_info_slug);
            Log::info(' ');

            /* start get detaila anime */

            $detail_anime_client = new Client();

            $crawler_detail_anime = $detail_anime_client->request('GET', $base_url.'/category/'.$anime_info_slug);

            $anime_detail_slug = substr($crawler_detail_anime->filter('head link[rel=canonical]')->attr('href'), $remove_category_string_for_slug);
            $result_anime_detail['anime_detail_slug'] = $anime_detail_slug;

            $anime_detail_image = $crawler_detail_anime->filter('.anime_info_body_bg img')->attr('src');
            $result_anime_detail['anime_detail_image'] = $anime_detail_image;

            $anime_detail_title = $crawler_detail_anime->filter('.anime_info_body_bg h1')->each(function ($node) {
                return $node->text();
            });
            $result_anime_detail['anime_detail_title'] = $anime_detail_title[0];

            $anime_detail_sub_category = $crawler_detail_anime->filter('.anime_info_body_bg a')->eq(1)->each(function ($node) {
                return $node->text();
            });
            $result_anime_detail['anime_detail_sub_category'] = $anime_detail_sub_category[0];

            $anime_detail_sub_category_slug = substr($crawler_detail_anime->filter('.anime_info_body_bg a')->eq(1)->attr('href'), $remove_sub_category_string_for_slug);
            $result_anime_detail['anime_detail_sub_category_slug'] = $anime_detail_sub_category_slug;

            $anime_detail_description = $crawler_detail_anime->filter('.anime_info_body_bg p')->eq(2)->each(function ($node) use ($plot_summary_len) {
                return substr($node->text(), $plot_summary_len);
            });
            $result_anime_detail['anime_detail_description'] = $anime_detail_description[0];

            $anime_detail_genre = $crawler_detail_anime->filter('.anime_info_body_bg a')->extract(array('title'));

            unset($anime_detail_genre[0]);
            unset($anime_detail_genre[1]);

            $result_anime_detail['anime_detail_genre'] = $anime_detail_genre;

            $anime_detail_released = $crawler_detail_anime->filter('.anime_info_body_bg p')->eq(4)->each(function ($node) use ($released_len) {
                return substr($node->text(), $released_len);
            });
            $result_anime_detail['anime_detail_released'] = $anime_detail_released[0];

            $anime_detail_status = $crawler_detail_anime->filter('.anime_info_body_bg p')->eq(5)->each(function ($node) use ($ongoing_status_len) {
                return substr($node->text(), $ongoing_status_len);
            });
            $result_anime_detail['anime_detail_status'] = $anime_detail_status[0];

            $crawler_detail_anime->clear();
            unset($crawler_detail_anime);
            unset($detail_anime_client);

            /* end get detaila anime */

            // Title
            $anime_episode_title = $crawler_anime_episode_detail->filter('.title_name h2')->each(function ($node) {
                return $node->text();
            });

            $result_anime_episode['anime_episode_title'] = $anime_episode_title[0];

            // Category
            $anime_episode_category = $crawler_anime_episode_detail->filter('.anime_video_body_cate a')->attr('title');
            $result_anime_episode['anime_episode_category'] = $anime_episode_category;
            // Log::info('isi category: '.$category);
            // Log::info(' ');

            // Category Slug
            $anime_episode_category_slug = substr($crawler_anime_episode_detail->filter('.anime_video_body_cate a')->attr('href'), $remove_sub_category_string_for_slug);
            $result_anime_episode['anime_episode_category_slug'] = $anime_episode_category_slug;
            // Log::info('isi category slug: '.$category_slug);
            // Log::info(' ');

            // Anime Detail Slug
            $anime_episode_detail_slug = substr($crawler_anime_episode_detail->filter('head link[rel=canonical]')->attr('href'), 1);
            $result_anime_episode['anime_episode_detail_slug'] = $anime_episode_detail_slug;
            // Log::info('isi link anime detail: '.$anime_detail_slug);
            // Log::info(' ');

            // Streaming Links
            $streaming_links = $crawler_anime_episode_detail->filter('a[data-video]')->extract(array('data-video'));
            // Log::info('isi streaming links: '.json_encode($streaming_links));

            for ($i=0; $i < count($streaming_links); $i++) { 

                $findme   = 'https:';
                $pos = strpos($streaming_links[$i], $findme);

                // Note our use of ===. Simply, == would not work as expected
                // because the position of 'a' was the 0th (first) character.
                if ($pos === false) {
                    $streaming_links[$i] = 'https:'.$streaming_links[$i];
                }
            }

            // Streaming Link Names
            $streaming_link_names = $crawler_anime_episode_detail->filter('a[data-video]')->each(function ($node) use ($minus_remove_choose_this_server_string) {
                return substr($node->text(), 0, $minus_remove_choose_this_server_string);
            });

            $result_anime_episode_streaming[0] = $streaming_links;
            $result_anime_episode_streaming[1] = $streaming_link_names;

            // Cari link dengan nama tombol download, lalu di pencet
            $link = $crawler_anime_episode_detail->selectLink('Download')->link();
            $crawler_download = $recent_release_client->click($link);

            // Download Links
            $download_links = $crawler_download->filter('.dowload a')->extract(array('href'));

            // Download Link Names
            $download_text_names = $crawler_download->filter('.dowload a')->each(function ($node) {
                return $node->text();
            });

            $result_anime_episode_download[0] = $download_links;
            $result_anime_episode_download[1] = $download_text_names;

            $result_all_recent_release['anime_detail'] = $result_anime_detail;
            $result_all_recent_release['anime_episode_detail'] = $result_anime_episode;
            $result_all_recent_release['anime_episode_streaming'] = $result_anime_episode_streaming;
            $result_all_recent_release['anime_episode_download'] = $result_anime_episode_download;

            $crawler_anime_episode_detail->clear();
            unset($crawler_anime_episode_detail);

            $crawler_download->clear();
            unset($crawler_download);
            
            return $result_all_recent_release;
        });

        for ($i=0; $i < count($data_recent_releases); $i++) { 

            $video_id = '';

            $is_exist_video = VideoDescriptionModel::select('id')
            ->where('slug', $data_recent_releases[$i]['anime_detail']['anime_detail_slug'])
            ->where('delete', 0)
            ->first();

            if($is_exist_video != null){
                $video_id = $is_exist_video->id;
            } else {
                $result_detail_anime = $get_video = VideoDescriptionModel::create([
                    'user_id' => 1,
                    'sub_category' => $data_recent_releases[$i]['anime_detail']['anime_detail_sub_category'],
                    'sub_category_slug' => $data_recent_releases[$i]['anime_detail']['anime_detail_sub_category_slug'],
                    'img' => $data_recent_releases[$i]['anime_detail']['anime_detail_image'],
                    'slug' => $data_recent_releases[$i]['anime_detail']['anime_detail_slug'],
                    'title' => $data_recent_releases[$i]['anime_detail']['anime_detail_title'],
                    'description' => $data_recent_releases[$i]['anime_detail']['anime_detail_description'],
                    'released' => $data_recent_releases[$i]['anime_detail']['anime_detail_released'],
                    'status' => $data_recent_releases[$i]['anime_detail']['anime_detail_status'],
                    'delete' => 0 
                ]);

                $video_id = $result_detail_anime->id;
            }

            foreach ($data_recent_releases[$i]['anime_detail']['anime_detail_genre'] as $detail_anime_genre) {
                $is_exist_genre = ConnectVideoAndGenreModel::select('id')
                ->where('video_description_id', $video_id)
                ->where('genre_slug', str_slug($detail_anime_genre, '-'))
                ->where('delete', 0)
                ->first();

                if($is_exist_genre == null)
                {
                    ConnectVideoAndGenreModel::create([
                        'user_id' => 1,
                        'video_description_id' => $video_id,
                        'genre_slug' => str_slug($detail_anime_genre, '-'),
                        'genre_text' => $detail_anime_genre,
                        'delete' => 0
                    ]);
                }
            }

            $video_episode_id = '';

            $is_exist_video_episode = VideoEpisodeModel::select('GA_VideoEpisode.id')
            ->where('video_description_id', $video_id)
            ->where('slug', $data_recent_releases[$i]['anime_episode_detail']['anime_episode_detail_slug'])
            ->where('delete', 0)
            ->first();

            if($is_exist_video_episode != null) {
                $video_episode_id = $is_exist_video_episode->id;
            } else {
                $result_anime_episode_detail = VideoEpisodeModel::create([
                    'video_description_id' => $video_id,
                    'slug' => $data_recent_releases[$i]['anime_episode_detail']['anime_episode_detail_slug'],
                    'title' => $data_recent_releases[$i]['anime_episode_detail']['anime_episode_title'],
                    'delete' => 0
                ]);

                $video_episode_id = $result_anime_episode_detail->id;
            }
            
            if(count($data_recent_releases[$i]['anime_episode_streaming'][0]) == count($data_recent_releases[$i]['anime_episode_streaming'][1])) {

                for ($k=0; $k < count($data_recent_releases[$i]['anime_episode_streaming'][0]); $k++) { 

                    $is_exist_streaming_url = StreamingUrlModel::select('id')
                    ->where('video_episode_id', $video_episode_id)
                    ->where('url', $data_recent_releases[$i]['anime_episode_streaming'][0][$k])
                    ->first();

                    if($is_exist_streaming_url == null) {

                        StreamingUrlModel::create([
                            'video_episode_id' => $video_episode_id,
                            'button_name' => $data_recent_releases[$i]['anime_episode_streaming'][1][$k],
                            'url' => $data_recent_releases[$i]['anime_episode_streaming'][0][$k],
                            'delete' => 0
                        ]);
                    }
                }
            }

            if(count($data_recent_releases[$i]['anime_episode_download'][0]) == count($data_recent_releases[$i]['anime_episode_download'][1])) {
                
                for ($j=0; $j < count($data_recent_releases[$i]['anime_episode_download'][0]); $j++) { 

                    $is_exist_download_url = DownloadUrlModel::select('id')
                    ->where('video_episode_id', $video_episode_id)
                    ->where('url', $data_recent_releases[$i]['anime_episode_download'][0][$j])
                    ->first();

                    if($is_exist_download_url == null) {

                        DownloadUrlModel::create([
                            'video_episode_id' => $video_episode_id,
                            'button_name' => $data_recent_releases[$i]['anime_episode_download'][1][$j],
                            'url' => $data_recent_releases[$i]['anime_episode_download'][0][$j],
                            'delete' => 0
                        ]);
                    }
                }
            }
        }
        
        $crawler_recent_release->clear();
        unset($crawler_recent_release);
        unset($recent_release_client);

        $this->info('Get Recent Release Anime Success !');
    }
}
