<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth, Hash, DB, Log;

class UserModel extends Authenticatable
{
	protected $table = 'GA_Users';
	protected $primaryKey = 'id';
    protected $fillable = ['name', 'email', 'password', 'role', 'delete'];

    private $success_update_msg = 'Data berhasil di ubah.';
    private $success_add_msg = 'Data berhasil di tambahkan.';
    private $success_delete_msg = 'Data berhasil di hapus.';

    public function countAllActiveUser($role)
    {
    	if($role != null || $role != '')
    	{
    		$count_admin = $this->select('id')
	        ->where('role', $role)
	        ->where('delete', 0)
	        ->count();

	        return $count_admin;
    	}
    	else
    	{
            $count_admin = $this->select('id')
            ->where('delete', 0)
            ->count();

            return $count_admin;
    	}
    }

    public function countAllFilteredActiveUser($search, $role)
    {
    	if($role != null || $role != '')
    	{
    		$count_admin = $this->select('id')
	        ->where(function ($q) use($search) {
	            $q->where('id', 'like', '%'.$search.'%');
	            $q->orWhere('name', 'like', '%'.$search.'%');
	            $q->orWhere('email', 'like', '%'.$search.'%');
	            $q->orWhere('role', 'like', '%'.$search.'%');
	        })
	        ->where('role', $role)
	        ->where('delete', 0)
	        ->count();

	        return $count_admin;
    	}
    	else
    	{
            $count_admin = $this->select('id')
            ->where(function ($q) use($search) {
                $q->where('id', 'like', '%'.$search.'%');
                $q->orWhere('name', 'like', '%'.$search.'%');
                $q->orWhere('email', 'like', '%'.$search.'%');
                $q->orWhere('role', 'like', '%'.$search.'%');
            })
            ->where('delete', 0)
            ->count();

            return $count_admin;
    	}
    }

    public function getUser($start, $limit, $order, $dir, $role)
    {
    	if($role != null || $role != '')
    	{
    		$admin = $this->select('id', 'name', 'email', 'role')
	        ->where('role', $role)
	        ->where('delete', 0)
	        ->offset($start)
	        ->limit($limit)
	        ->orderBy($order,$dir)
	        ->get();

	    	return $admin;
    	}
    	else
    	{
            $admin = $this->select('id', 'name', 'email', 'role')
            ->where('delete', 0)
            ->offset($start)
            ->limit($limit)
            ->orderBy($order,$dir)
            ->get();

            return $admin;
    	}
    }

    public function getFilteredUser($search, $start, $limit, $order, $dir, $role)
    {
    	if($role != null || $role != '')
    	{
    		$admin = $this->select('id', 'name', 'email', 'role')
	        ->where(function ($q) use($search) {
	            $q->where('id', 'like', '%'.$search.'%');
	            $q->orWhere('name', 'like', '%'.$search.'%');
	            $q->orWhere('email', 'like', '%'.$search.'%');
	            $q->orWhere('role', 'like', '%'.$search.'%');
	        })
	        ->where('role', $role)
	        ->where('delete', 0)
	        ->offset($start)
	        ->limit($limit)
	        ->orderBy($order,$dir)
	        ->get();

	        return $admin;
    	}
    	else
    	{
            $admin = $this->select('id', 'name', 'email', 'role')
            ->where(function ($q) use($search) {
                $q->where('id', 'like', '%'.$search.'%');
                $q->orWhere('name', 'like', '%'.$search.'%');
                $q->orWhere('email', 'like', '%'.$search.'%');
                $q->orWhere('role', 'like', '%'.$search.'%');
            })
            ->where('delete', 0)
            ->offset($start)
            ->limit($limit)
            ->orderBy($order,$dir)
            ->get();

            return $admin;
    	}
    }

    public function getOneUser($id, $role)
    {
    	if($role != null || $role != '')
    	{
    		$admin = $this->select('id', 'name', 'email', 'role')
	    	->where('role', $role)
	    	->where('id', $id)
	    	->where('delete', 0)
	    	->first();

	    	return $admin;
    	}
    	else
    	{
            $admin = $this->select('id', 'name', 'email', 'role')
            ->where('id', $id)
            ->where('delete', 0)
            ->first();

            return $admin;
    	}
    }

    public function postAddUser($param)
    {
    	$result = [];

    	$final = DB::transaction(function () use($param, $result) {
		    $data = $this->create([
                'name' => $param['name'],
                'email' => $param['email'],
                'password' => Hash::make($param['password']),
                'role' => $param['role'],
		    	'delete' => 0
		    ]);

		   	$result[0] = $data->id;
		   	$result[1] = $this->success_add_msg;

		    return $result;
		});

		return $final;
    }

    public function postEditUser($param, $id)
    {
    	DB::transaction(function () use($param, $id) {
    		if($param['password'] != null || $param['password'] != '')
    		{
    			$this->where('id', $id)->where('delete', 0)->update([
			    	'name' => $param['name'],
	                'email' => $param['email'],
	                'password' => Hash::make($param['password']),
	                'role' => $param['role']
			    ]);
    		}
    		else
    		{
    			$this->where('id', $id)->where('delete', 0)->update([
			    	'name' => $param['name'],
	                'email' => $param['email'],
	                'role' => $param['role']
			    ]);
    		}
		});

		return $this->success_update_msg;
    }

    public function postDeleteUser($id)
    {
    	$result = [];

    	DB::transaction(function () use($id) {
		    $this->where('id', $id)->where('delete', 0)->update([
		    	'delete' => 1
		    ]);
		});

    	$result['message'] = $this->success_update_msg;

		return $result;
    }
}
