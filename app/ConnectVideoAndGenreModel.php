<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth, Hash, DB, Log;

class ConnectVideoAndGenreModel extends Model
{
	protected $table = 'GA_ConnectionVideoDescAndGenre';
	protected $primaryKey = 'id';
    protected $fillable = ['user_id', 'video_description_id', 'genre_slug', 'genre_text', 'delete'];

    private $success_update_msg = 'Data berhasil di ubah.';
    private $success_add_msg = 'Data berhasil di tambahkan.';
    private $success_delete_msg = 'Data berhasil di hapus.';

    public function countAllActiveAnimeGenre($anime_id)
    {
        $count_source_host = $this->select('id')
        ->where('video_description_id', $anime_id)
        ->where('delete', 0)
        ->count();

        return $count_source_host;
    }

    public function countAllFilteredActiveAnimeGenre($search, $anime_id)
    {
        $count_source_host = $this->select('id')
        ->where(function ($q) use($search) {
            $q->where('id', 'like', '%'.$search.'%');
            $q->orWhere('genre_text', 'like', '%'.$search.'%');
        })
        ->where('video_description_id', $anime_id)
        ->where('delete', 0)
        ->count();

        return $count_source_host;
    }

    public function getAnimeGenre($start, $limit, $order, $dir, $anime_id)
    {
    	$source_host = $this->select('id', 'genre_text')
    	->where('video_description_id', $anime_id)
        ->where('delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

    	return $source_host;
    }

    public function getFilteredAnimeGenre($search, $start, $limit, $order, $dir, $anime_id)
    {
        $source_host = $this->select('id', 'genre_text')
        ->where(function ($q) use($search) {
            $q->where('id', 'like', '%'.$search.'%');
            $q->orWhere('genre_text', 'like', '%'.$search.'%');
        })
        ->where('video_description_id', $anime_id)
        ->where('delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $source_host;
    }

    public function getOneAnimeGenre($id, $anime_id)
    {
    	$source_host = $this->select('id', 'genre_text', 'genre_slug')
    	->where('id', $id)
    	->where('video_description_id', $anime_id)
    	->where('delete', 0)
    	->first();

    	return $source_host;
    }

    public function postAddAnimeGenre($param, $anime_id)
    {
    	$result = [];

    	$final = DB::transaction(function () use($param, $result, $anime_id) {
		    $data = $this->create([
		    	'user_id' => Auth::user()->id,
		    	'video_description_id' => $anime_id,
                'genre_slug' => str_slug($param['genre_slug']),
                'genre_text' => $param['genre_text'],
		    	'delete' => 0
		    ]);

		   	$result[0] = $data->id;
		   	$result[1] = $this->success_add_msg;

		    return $result;
		});

		return $final;
    }

    public function postEditAnimeGenre($param, $id, $anime_id)
    {
    	DB::transaction(function () use($param, $id, $anime_id) {
		    $this->where('id', $id)->where('delete', 0)->update([
		    	'video_description_id' => $anime_id,
                'genre_slug' => str_slug($param['genre_slug']),
                'genre_text' => $param['genre_text']
		    ]);
		});

		return $this->success_update_msg;
    }

    public function postDeleteAnimeGenre($id, $anime_id)
    {
    	$result = [];

    	DB::transaction(function () use($id, $anime_id) {
		    $this->where('id', $id)->where('video_description_id', $anime_id)->where('delete', 0)->update([
		    	'delete' => 1
		    ]);
		});

    	$result['message'] = $this->success_update_msg;

		return $result;
    }

    public function getFEGenre()
    {
        $genre = $this->select('genre_text', 'genre_slug')
        ->where('delete', 0)
        ->groupBy('genre_slug')
        ->orderBy('genre_text','asc')
        ->get();

        return $genre;
    }
}
