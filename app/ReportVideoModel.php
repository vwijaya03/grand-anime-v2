<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportVideoModel extends Model
{
    protected $table = 'GA_ReportVideo';
	protected $primaryKey = 'id';
    protected $fillable = ['video_episode_id', 'url_video', 'problem', 'other_problem', 'delete'];
}
