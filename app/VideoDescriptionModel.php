<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth, Hash, DB, Log;

class VideoDescriptionModel extends Model
{
    protected $table = 'GA_Video';
	protected $primaryKey = 'id';
    protected $fillable = ['user_id', 'sub_category', 'sub_category_slug', 'img', 'slug', 'title', 'description', 'released', 'status', 'delete'];

    private $success_update_msg = 'Data berhasil di ubah.';
    private $success_add_msg = 'Data berhasil di tambahkan.';
    private $success_delete_msg = 'Data berhasil di hapus.';

    public function countAllActiveAnime()
    {
        $count_source_host = $this->select('id')
        ->where('delete', 0)
        ->count();

        return $count_source_host;
    }

    public function countAllFilteredActiveAnime($search)
    {
        $count_source_host = $this->select('id')
        ->where(function ($q) use($search) {
            $q->where('id', 'like', '%'.$search.'%');
            $q->orWhere('sub_category', 'like', '%'.$search.'%');
            $q->orWhere('title', 'like', '%'.$search.'%');
            $q->orWhere('released', 'like', '%'.$search.'%');
            $q->orWhere('status', 'like', '%'.$search.'%');
        })
        ->where('delete', 0)
        ->count();

        return $count_source_host;
    }

    public function getAnime($start, $limit, $order, $dir)
    {
    	$source_host = $this->select('id', 'sub_category', 'title', 'released', 'status')
        ->where('delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

    	return $source_host;
    }

    public function getFilteredAnime($search, $start, $limit, $order, $dir)
    {
        $source_host = $this->select('id', 'sub_category', 'title', 'released', 'status')
        ->where(function ($q) use($search) {
            $q->where('id', 'like', '%'.$search.'%');
            $q->orWhere('sub_category', 'like', '%'.$search.'%');
            $q->orWhere('title', 'like', '%'.$search.'%');
            $q->orWhere('released', 'like', '%'.$search.'%');
            $q->orWhere('status', 'like', '%'.$search.'%');
        })
        ->where('delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $source_host;
    }

    public function getOneAnime($id)
    {
    	$source_host = $this->select('id', 'sub_category', 'sub_category_slug', 'img', 'slug', 'title', 'description', 'released', 'status')->where('id', $id)->where('delete', 0)->first();
    	return $source_host;
    }

    public function postAddAnime($param)
    {
    	$result = [];

    	$final = DB::transaction(function () use($param, $result) {
		    $data = $this->create([
                'user_id' => Auth::user()->id,
		    	'sub_category' => $param['sub_category'],
		    	'sub_category_slug' => str_slug($param['sub_category']),
                'img' => $param['img'],
                'slug' => str_slug($param['title']),
                'title' => $param['title'],
                'description' => $param['description'],
		    	'released' => $param['released'],
		    	'status' => $param['status'],
		    	'delete' => 0
		    ]);

		   	$result[0] = $data->id;
		   	$result[1] = $this->success_add_msg;

		    return $result;
		});

		return $final;
    }

    public function postEditAnime($param, $id)
    {
    	DB::transaction(function () use($param, $id) {
		    $this->where('id', $id)->where('delete', 0)->update([
		    	'sub_category' => $param['sub_category'],
		    	'sub_category_slug' => str_slug($param['sub_category']),
                'img' => $param['img'],
                'slug' => str_slug($param['title']),
                'title' => $param['title'],
                'description' => $param['description'],
		    	'released' => $param['released'],
		    	'status' => $param['status'],
		    ]);
		});

		return $this->success_update_msg;
    }

    public function postDeleteAnime($id)
    {
    	$result = [];

    	DB::transaction(function () use($id) {
		    $this->where('id', $id)->where('delete', 0)->update([
		    	'delete' => 1
		    ]);
		});

    	$result['message'] = $this->success_update_msg;

		return $result;
    }
}
