<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryModel extends Model
{
    protected $table = 'GA_Category';
	protected $primaryKey = 'id';
    protected $fillable = ['slug', 'name', 'delete'];
}
