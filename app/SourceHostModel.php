<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth, Hash, DB, Log;

class SourceHostModel extends Model
{
    protected $table = 'GA_SourceHost';
	protected $primaryKey = 'id';
    protected $fillable = ['protocol', 'url', 'additional_url', 'recent_page', 'episode_start_page', 'episode_end_page', 'tipe', 'delete'];

    private $success_update_msg = 'Data berhasil di ubah.';
    private $success_add_msg = 'Data berhasil di tambahkan.';
    private $success_delete_msg = 'Data berhasil di hapus.';

    public function countAllActiveSourceHost()
    {
        $count_source_host = $this->select('id')
        ->where('delete', 0)
        ->count();

        return $count_source_host;
    }

    public function countAllFilteredActiveSourceHost($search)
    {
        $count_source_host = $this->select('id')
        ->where(function ($q) use($search) {
            $q->where('id', 'like', '%'.$search.'%');
            $q->orWhere('protocol', 'like', '%'.$search.'%');
            $q->orWhere('url', 'like', '%'.$search.'%');
            $q->orWhere('additional_url', 'like', '%'.$search.'%');
            $q->orWhere('recent_page', 'like', '%'.$search.'%');
            $q->orWhere('episode_start_page', 'like', '%'.$search.'%');
            $q->orWhere('episode_end_page', 'like', '%'.$search.'%');
            $q->orWhere('tipe', 'like', '%'.$search.'%');
        })
        ->where('delete', 0)
        ->count();

        return $count_source_host;
    }

    public function getSourceHost($start, $limit, $order, $dir)
    {
    	$source_host = $this->select('id', 'protocol', 'url', 'additional_url', 'recent_page', 'episode_start_page', 'episode_end_page', 'tipe')
        ->where('delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

    	return $source_host;
    }

    public function getFilteredSourceHost($search, $start, $limit, $order, $dir)
    {
        $source_host = $this->select('id', 'protocol', 'url', 'additional_url', 'recent_page', 'episode_start_page', 'episode_end_page', 'tipe')
        ->where(function ($q) use($search) {
            $q->where('id', 'like', '%'.$search.'%');
            $q->orWhere('protocol', 'like', '%'.$search.'%');
            $q->orWhere('url', 'like', '%'.$search.'%');
            $q->orWhere('additional_url', 'like', '%'.$search.'%');
            $q->orWhere('recent_page', 'like', '%'.$search.'%');
            $q->orWhere('episode_start_page', 'like', '%'.$search.'%');
            $q->orWhere('episode_end_page', 'like', '%'.$search.'%');
            $q->orWhere('tipe', 'like', '%'.$search.'%');
        })
        ->where('delete', 0)
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        return $source_host;
    }

    public function getOneSourceHost($id)
    {
    	$source_host = $this->select('id', 'protocol', 'url', 'additional_url', 'recent_page', 'episode_start_page', 'episode_end_page', 'tipe')->where('id', $id)->where('delete', 0)->first();
    	return $source_host;
    }

    public function postAddSourceHost($param)
    {
    	$result = [];

    	$final = DB::transaction(function () use($param, $result) {
		    $data = $this->create([
		    	'protocol' => $param['protocol'],
		    	'url' => $param['url'],
                'additional_url' => $param['additional_url'],
                'recent_page' => $param['recent_page'],
                'episode_start_page' => $param['episode_start_page'],
                'episode_end_page' => $param['episode_end_page'],
		    	'tipe' => $param['tipe'],
		    	'delete' => 0
		    ]);

		   	$result[0] = $data->id;
		   	$result[1] = $this->success_add_msg;

		    return $result;
		});

		return $final;
    }

    public function postEditSourceHost($param, $id)
    {
    	DB::transaction(function () use($param, $id) {
		    $this->where('id', $id)->where('delete', 0)->update([
		    	'protocol' => $param['protocol'],
		    	'url' => $param['url'],
		    	'additional_url' => $param['additional_url'],
                'recent_page' => $param['recent_page'],
                'episode_start_page' => $param['episode_start_page'],
                'episode_end_page' => $param['episode_end_page'],
                'tipe' => $param['tipe']
		    ]);
		});

		return $this->success_update_msg;
    }

    public function postDeleteSourceHost($id)
    {
    	$result = [];

    	DB::transaction(function () use($id) {
		    $this->where('id', $id)->where('delete', 0)->update([
		    	'delete' => 1
		    ]);
		});

    	$result['message'] = $this->success_update_msg;

		return $result;
    }
}
