<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth, Hash, DB, Log;

class VideoEpisodeModel extends Model
{
    protected $table = 'GA_VideoEpisode';
	protected $primaryKey = 'id';
    protected $fillable = ['video_description_id', 'slug', 'title', 'delete'];

    private $success_update_msg = 'Data berhasil di ubah.';
    private $success_add_msg = 'Data berhasil di tambahkan.';
    private $success_delete_msg = 'Data berhasil di hapus.';

    public function countAllActiveAnimeEpisode($anime_id)
    {
        $count_anime_episode = $this->select('id')
        ->where('video_description_id', $anime_id)
        ->where('delete', 0)
        ->count();

        return $count_anime_episode;
    }

    public function countAllFilteredActiveAnimeEpisode($search, $anime_id)
    {
        $count_anime_episode = $this->select('id')
        ->where(function ($q) use($search) {
            $q->where('id', 'like', '%'.$search.'%');
            $q->orWhere('title', 'like', '%'.$search.'%');
        })
        ->where('video_description_id', $anime_id)
        ->where('delete', 0)
        ->count();

        return $count_anime_episode;
    }

    public function getAnimeEpisode($start, $limit, $order, $dir, $anime_id)
    {
    	$anime_episode = $this->select('id', 'title')
    	->where('video_description_id', $anime_id)
        ->where('delete', 0)
        ->offset($start)
        ->limit($limit)
        // ->orderBy($order,$dir)
        ->orderByRaw('LENGTH('.$order.') '.$dir.', '.$order.' '.$dir.' ')
        ->get();

    	return $anime_episode;
    }

    public function getFilteredAnimeEpisode($search, $start, $limit, $order, $dir, $anime_id)
    {
        $anime_episode = $this->select('id', 'title')
        ->where(function ($q) use($search) {
            $q->where('id', 'like', '%'.$search.'%');
            $q->orWhere('title', 'like', '%'.$search.'%');
        })
        ->where('video_description_id', $anime_id)
        ->where('delete', 0)
        ->offset($start)
        ->limit($limit)
        // ->orderBy($order,$dir)
        ->orderByRaw('LENGTH('.$order.') '.$dir.', '.$order.' '.$dir.' ')
        ->get();

        return $anime_episode;
    }

    public function getOneAnimeEpisode($id, $anime_id)
    {
    	$anime_episode = $this->select('id', 'title', 'slug')
    	->where('id', $id)
    	->where('video_description_id', $anime_id)
    	->where('delete', 0)
    	->first();

    	return $anime_episode;
    }

    public function getOneAnimeEpisodeWithAnimeEpisodeIdOnly($anime_episode_id)
    {
        $anime_episode = $this->select('id', 'title', 'slug')
        ->where('id', $anime_episode_id)
        ->where('delete', 0)
        ->first();

        return $anime_episode;
    }

    public function postAddAnimeEpisode($param, $anime_id)
    {
    	$result = [];

    	$final = DB::transaction(function () use($param, $result, $anime_id) {
		    $data = $this->create([
		    	'video_description_id' => $anime_id,
                'slug' => str_slug($param['slug']),
                'title' => $param['title'],
		    	'delete' => 0
		    ]);

		   	$result[0] = $data->id;
		   	$result[1] = $this->success_add_msg;

		    return $result;
		});

		return $final;
    }

    public function postEditAnimeEpisode($param, $id, $anime_id)
    {
    	DB::transaction(function () use($param, $id, $anime_id) {
		    $this->where('id', $id)->where('delete', 0)->update([
		    	'video_description_id' => $anime_id,
                'slug' => str_slug($param['slug']),
                'title' => $param['title']
		    ]);
		});

		return $this->success_update_msg;
    }

    public function postDeleteAnimeEpisode($id, $anime_id)
    {
    	$result = [];

    	DB::transaction(function () use($id, $anime_id) {
		    $this->where('id', $id)->where('video_description_id', $anime_id)->where('delete', 0)->update([
		    	'delete' => 1
		    ]);
		});

    	$result['message'] = $this->success_update_msg;

		return $result;
    }
}
