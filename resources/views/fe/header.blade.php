<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7 no-js" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8 no-js" lang="en-US">
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html lang="en" class="no-js">
<head>
	<!-- Basic need -->
	<title>Grand Anime</title>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<link rel="profile" href="#">

    <!--Google Font-->
    <link rel="stylesheet" href='http://fonts.googleapis.com/css?family=Dosis:400,700,500|Nunito:300,400,600' />
	<!-- Mobile specific meta -->
	<meta name=viewport content="width=device-width, initial-scale=1">
	<meta name="format-detection" content="telephone-no">

	<!-- CSS files -->
	<link rel="stylesheet" href="{{ URL::asset('/fe/css/plugins.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('/fe/css/style.css') }}">

	<!-- Icon -->
	<link rel="icon" href="{{ URL::asset('/fe/images/icon.png') }}">

</head>
<body>
<!--preloading-->
<div id="preloader">
    <img class="logo" src="{{ URL::asset('/fe/images/logo1.png') }}" alt="" width="119" height="58">
    <div id="status">
        <span></span>
        <span></span>
    </div>
</div>
<!--end of preloading-->

<!-- BEGIN | Header -->
<header class="ht-header">
	<div class="container">
		<nav class="navbar navbar-default navbar-custom">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header logo">
				    <div class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					    <span class="sr-only">Toggle navigation</span>
					    <div id="nav-icon1">
							<span></span>
							<span></span>
							<span></span>
						</div>
				    </div>
				    <a href="{{ url('/') }}"><img class="logo" src="{{ URL::asset('/fe/images/logo1.png') }}" alt="" width="119" height="58"></a>
			    </div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse flex-parent" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav flex-child-menu menu-left">
						<li class="hidden">
							<a href="#page-top"></a>
						</li>
						<li class="dropdown first">
							<a href="{{ url('/') }}" class="btn btn-default">
							Home</i>
							</a>
						</li>
						<li class="dropdown first">
							<a href="{{ url('/anime-list') }}" class="btn btn-default">
							Anime List</i>
							</a>
						</li>
						<li class="dropdown first">
							<a href="{{ url('/new-season-anime') }}" class="btn btn-default">
							New Season</i>
							</a>
						</li>
						<li class="dropdown first">
							<a href="{{ url('/movies-anime') }}" class="btn btn-default">
							Movies</i>
							</a>
						</li>
					</ul>
					<ul class="nav navbar-nav flex-child-menu menu-right">
						<!-- <li class="dropdown first">
							<a class="btn btn-default dropdown-toggle lv1" data-toggle="dropdown" data-hover="dropdown">
							pages <i class="fa fa-angle-down" aria-hidden="true"></i>
							</a>
							<ul class="dropdown-menu level1">
								<li><a href="landing.html">Landing</a></li>
								<li><a href="404.html">404 Page</a></li>
								<li class="it-last"><a href="comingsoon.html">Coming soon</a></li>
							</ul>
						</li>       -->          
						<li><a href="#">Contact Us</a></li>
						<!-- <li class="loginLink"><a href="#">LOG In</a></li>
						<li class="btn signupLink"><a href="#">sign up</a></li> -->
					</ul>
				</div>
			<!-- /.navbar-collapse -->
	    </nav>
	    
	    <!-- top search form -->
	    <form action="{{ url('/search-anime') }}" method="GET">
		    <div class="top-search">
				<input type="text" placeholder="Search for a anime that you are looking for" name="q" autocomplete="off" required>
				<input class="submit" type="submit" value="" style="background: url('{{ url('/') }}/fe/images/uploads/topsearch.png') no-repeat right 20px center; padding-right: 50px">
		    </div>
	    </form>
	</div>
</header>
<!-- END | Header -->

@yield('content')

<!-- footer section-->
<footer class="ht-footer">
	<div class="ft-copyright">
		<div class="ft-left">
			<p>© {{ date('Y') }} Grand Anime. All Rights Reserved.</p>
		</div>
		<div class="backtotop">
			<p><a href="#" id="back-to-top">Back to top  <i class="ion-ios-arrow-thin-up"></i></a></p>
		</div>
	</div>
</footer>
<!-- end of footer section-->

<script src="{{ URL::asset('/fe/js/jquery.js') }}"></script>
<script src="{{ URL::asset('/fe/js/plugins.js') }}"></script>
<script src="{{ URL::asset('/fe/js/plugins2.js') }}"></script>
<script src="{{ URL::asset('/fe/js/custom.js') }}"></script>
</body>
</html>