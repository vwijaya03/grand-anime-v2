@extends('fe/header')
@section('content')
<div class="separation common-separation-home">
	
</div>

<div class="page-recent-release">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-sm-12 col-xs-12">
				<div class="title-hd">
					<h2>Sub Category Anime</h2>
				</div>
				<div class="flex-wrap-movielist mv-grid-fw">

					@foreach($animes as $anime)
					<div class="movie-item-style-2 movie-item-style-1">
						<img src="{{ $anime->img }}" alt="" style="width: 170px !important; height: 261px !important;">
						<div class="hvr-inner">
            				<a  href="{{ url('/anime/'.$anime->slug) }}"> Read more <i class="ion-android-arrow-dropright"></i> </a>
            			</div>
						<div class="mv-item-infor">
							<h6><a href="{{ url('/anime/'.$anime->slug) }}">{{ $anime->title }}</a></h6>
						</div>
					</div>
					@endforeach
				</div>		
				<div class="topbar-filter">
					<label></label>
					
					{{ $animes->links('pagination') }}
				</div>
			</div>
			<div class="col-md-4 col-sm-12 col-xs-12">
				<div class="sidebar">
					<div class="sb-facebook sb-it">
						<h4 class="sb-title">Like Our Facebook Page</h4>
						<div class="fb-page" data-href="https://www.facebook.com/grandanime.net/" data-tabs="timeline" data-width="340" data-height="200" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><blockquote cite="https://www.facebook.com/grandanime.net/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/grandanime.net/">FairyTail99</a></blockquote></div>
					</div>
					<div class="ads">
						<img src="{{ URL::asset('/fe/images/uploads/ads1.png') }}" alt="">
					</div>
					<div class="ads">
						<img src="{{ URL::asset('/fe/images/uploads/ads1.png') }}" alt="">
					</div>
					<div class="ads">
						<img src="{{ URL::asset('/fe/images/uploads/ads1.png') }}" alt="">
					</div>
					<div class="ads">
						<img src="{{ URL::asset('/fe/images/uploads/ads1.png') }}" alt="">
					</div>
					<div class="ads">
						<img src="{{ URL::asset('/fe/images/uploads/ads1.png') }}" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.11&appId=1457791097570317';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
@endsection