@extends('fe/header')
@section('content')

<div class="hero mv-single-hero">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<!-- <h1> movie listing - list</h1>
				<ul class="breadcumb">
					<li class="active"><a href="#">Home</a></li>
					<li> <span class="ion-ios-arrow-right"></span> movie listing</li>
				</ul> -->
			</div>
		</div>
	</div>
</div>

<div class="page-single movie-single movie_single">
	<div class="container">
		<div class="row ipad-width2">
			<div class="col-md-4 col-sm-12 col-xs-12">
				<div class="movie-img sticky-sb">
					<img src="{{ $anime->img }}" alt="">
					<div class="movie-btn">	
						<div class="ads">
							<img src="{{ URL::asset('/fe/images/uploads/ads1.png') }}" alt="">
						</div>
						<div class="ads">
							<img src="{{ URL::asset('/fe/images/uploads/ads1.png') }}" alt="">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-8 col-sm-12 col-xs-12">
				<div class="movie-single-ct main-content">
					<h1 class="bd-hd">{{ $anime->title }}</h1>
					<div class="social-btn">
						<a href="#" class="parent-btn"><i class="ion-heart"></i> Add to Favorite</a>
						<div class="hover-bnt">
							<a href="#" class="parent-btn"><i class="ion-android-share-alt"></i>share</a>
							<div class="hvr-item">
								<a href="#" class="hvr-grow"><i class="ion-social-facebook"></i></a>
								<a href="#" class="hvr-grow"><i class="ion-social-twitter"></i></a>
								<a href="#" class="hvr-grow"><i class="ion-social-googleplus"></i></a>
								<a href="#" class="hvr-grow"><i class="ion-social-youtube"></i></a>
							</div>
						</div>		
					</div>
					<div class="movie-tabs">
						<div class="tabs">
							<ul class="tab-links tabs-mv">
								<li class="active"><a href="#overview">Overview</a></li>
							</ul>
						    <div class="tab-content">
						        <div id="overview" class="tab active">
						            <div class="row">
						            	<div class="col-md-8 col-sm-12 col-xs-12">
						            		<p>{{ $anime->description }}</p>

						            		<div class="row">
												<!-- //== -->
												<div class="title-hd-sm">
													<h4>Episodes List</h4>
												</div>
												<div class="mvcast-item">	
													@foreach($anime_episodes as $anime_episode)
													<div class="cast-it">
														<div class="cast-left">
															<a href="{{ url('/'.$anime_episode->slug) }}">{{ $anime_episode->title }}</a>
														</div>
													</div>
													@endforeach
												</div>

												<div class="topbar-filter">
													<label></label>
													
													{{ $anime_episodes->links('pagination') }}
												</div>
												<!-- //== -->
								            </div>
						            	</div>
						            	<div class="col-md-4 col-xs-12 col-sm-12">
						            		<div class="sb-it">
						            			<h6>Type</h6>
						            			<p class="tags">
													<span class="time"><a href="{{ url('/sub-category/'.$anime->sub_category_slug) }}">{{ $anime->sub_category }}</a></span>
						            			</p>
						            		</div>
						            		<div class="sb-it">
						            			<h6>Genre:</h6>
						            			<p class="tags">
						            				@foreach($genres as $genre)
						            				<span class="time"><a href="{{ url('/genre/'.$genre->genre_slug) }}">{{ $genre->genre_text }}</a></span>
													@endforeach
						            			</p>
						            		</div>
						            		<div class="sb-it">
						            			<h6>Released</h6>
						            			<p>{{ $anime->released }}</p>
						            		</div>
						            		<div class="sb-it">
						            			<h6>Status</h6>
						            			<p>{{ $anime->status }}</p>
						            		</div>
						            		<div class="ads">
												<img src="{{ URL::asset('/fe/images/uploads/ads1.png') }}" alt="">
											</div>
											<div class="ads">
												<img src="{{ URL::asset('/fe/images/uploads/ads1.png') }}" alt="">
											</div>
											<div class="ads">
												<img src="{{ URL::asset('/fe/images/uploads/ads1.png') }}" alt="">
											</div>
						            	</div>
						            </div>
						        </div>
						    </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection