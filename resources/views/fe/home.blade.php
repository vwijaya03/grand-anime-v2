@extends('fe/header')
@section('content')
<div class="separation common-separation-home">
	
</div>

<div class="page-recent-release">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-sm-12 col-xs-12">
				<div class="title-hd">
					<h2>Recent Release Anime</h2>
					<a href="{{ url('/recent-release-anime') }}" class="viewall">View all <i class="ion-ios-arrow-right"></i></a>
				</div>
				<div class="flex-wrap-movielist mv-grid-fw">

					@foreach($anime_episodes as $anime_episode)
					<div class="movie-item-style-2 movie-item-style-1">
						<img src="{{ $anime_episode->anime_img }}" alt="" style="width: 170px !important; height: 261px !important;">
						<div class="hvr-inner">
            				<a  href="{{ url('/'.$anime_episode->anime_episode_slug) }}"> Read more <i class="ion-android-arrow-dropright"></i> </a>
            			</div>
						<div class="mv-item-infor">
							<h6><a href="{{ url('/'.$anime_episode->anime_episode_slug) }}">{{ $anime_episode->anime_episode_title }}</a></h6>
						</div>
					</div>
					@endforeach
				</div>
			</div>
			<div class="col-md-4 col-sm-12 col-xs-12">
				<div class="sidebar">
					<div class="sb-facebook sb-it">
						<h4 class="sb-title">Like Our Facebook Page</h4>
						<div class="fb-page" data-href="https://www.facebook.com/grandanime.net/" data-tabs="timeline" data-width="340" data-height="200" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><blockquote cite="https://www.facebook.com/grandanime.net/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/grandanime.net/">FairyTail99</a></blockquote></div>
					</div>
					<div class="ads">
						<img src="{{ URL::asset('/fe/images/uploads/ads1.png') }}" alt="">
					</div>
					<div class="ads">
						<img src="{{ URL::asset('/fe/images/uploads/ads1.png') }}" alt="">
					</div>
					<div class="ads">
						<img src="{{ URL::asset('/fe/images/uploads/ads1.png') }}" alt="">
					</div>
					<div class="ads">
						<img src="{{ URL::asset('/fe/images/uploads/ads1.png') }}" alt="">
					</div>
					<div class="ads">
						<img src="{{ URL::asset('/fe/images/uploads/ads1.png') }}" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="movie-items">
	<div class="container">
		<div class="row ipad-width">
			<div class="col-md-8">
				<div class="title-hd">
					<h2>On Going Anime</h2>
					<a href="{{ url('/ongoing-anime') }}" class="viewall">View all <i class="ion-ios-arrow-right"></i></a>
				</div>
				<div class="tabs">
					<ul class="tab-links">
						<li class="active"><a href="#ongoing">#OnGoing</a></li>                     
					</ul>
				    <div class="tab-content">
				        <div id="ongoing" class="tab active">
				            <div class="row">
				            	<div class="slick-multiItem">
				            		@foreach($ongoing_animes as $ongoing_anime)
				            		<div class="slide-it">
				            			<div class="movie-item">
					            			<div class="mv-img">
					            				<img src="{{ $ongoing_anime->img }}" alt="" width="185" height="284">
					            			</div> 
					            			<div class="hvr-inner">
					            				<a  href="{{ url('/anime/'.$ongoing_anime->slug) }}"> Read more <i class="ion-android-arrow-dropright"></i> </a>
					            			</div>
					            			<div class="title-in">
					            				<h6><a href="{{ url('/anime/'.$ongoing_anime->slug) }}">{{ $ongoing_anime->title }}</a></h6>
					            			</div>
					            		</div>
				            		</div>
				            		@endforeach
				            	</div>
				            </div>
				        </div>
				    </div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="sidebar">
					<div class="celebrities">
						<h4 class="sb-title">Genres</h4>
						<div class="title-in">
		    				<div class="cate">
		    					@foreach($genres as $genre)
		    						<span class="blue"><a href="{{ url('/genre/'.$genre->genre_slug) }}">{{ $genre->genre_text }}</a></span> 
		    					@endforeach
		    				</div>
		    			</div>
					</div>
					<div class="ads">
						<img src="{{ URL::asset('/fe/images/uploads/ads1.png') }}" alt="" width="336" height="296">
					</div>
					<!-- <div class="celebrities">
						<h4 class="sb-title">featured celebrity</h4>
						<div class="celeb-item">
							<a href="#"><img src="{{ URL::asset('/fe/images/uploads/ava1.jpg') }}" alt=""></a>
							<div class="celeb-author">
								<h6><a href="#">Samuel N. Jack</a></h6>
								<span>Actor</span>
							</div>
						</div>
						<div class="celeb-item">
							<a href="#"><img src="{{ URL::asset('/fe/images/uploads/ava2.jpg') }}" alt=""></a>
							<div class="celeb-author">
								<h6><a href="#">Benjamin Carroll</a></h6>
								<span>Actor</span>
							</div>
						</div>
						<div class="celeb-item">
							<a href="#"><img src="{{ URL::asset('/fe/images/uploads/ava3.jpg') }}" alt=""></a>
							<div class="celeb-author">
								<h6><a href="#">Beverly Griffin</a></h6>
								<span>Actor</span>
							</div>
						</div>
						<div class="celeb-item">
							<a href="#"><img src="{{ URL::asset('/fe/images/uploads/ava4.jpg') }}" alt=""></a>
							<div class="celeb-author">
								<h6><a href="#">Justin Weaver</a></h6>
								<span>Actor</span>
							</div>
						</div>
					</div> -->
				</div>
			</div>
		</div>
	</div>
</div>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.11&appId=1457791097570317';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
@endsection