@extends('admin/header')

@section('content')

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-xs-12 mb-2">
                <h3 class="content-header-title mb-0">Data Anime Genre</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-xs-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url($url_admin.'/dashboard') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item"><a href="{{ url($url_admin.'/anime-genre-with-anime-id/'.$anime_id) }}">Anime Genre</a>
                            </li>
                            <li class="breadcrumb-item active">Tambah Anime Genre Baru
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body"><!-- HTML (DOM) sourced data -->
        	<section id="basic-form-layouts">
				<div class="row match-height">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title" id="basic-layout-form">{{ $anime->title }} <br><br> Tambah Anime Genre Baru</h4>
								<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
								<div class="heading-elements">
									<ul class="list-inline mb-0">
										<li><a data-action="collapse"><i class="ft-minus"></i></a></li>
										<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="card-body collapse in">
								<div class="card-block">

									@if ($errors->has('genre_text'))
										<div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<strong>Genre</strong> tidak boleh kosong !
										</div>
									@endif

									@if ($errors->has('genre_slug'))
										<div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<strong>Genre Slug</strong> tidak boleh kosong !
										</div>
									@endif

									@if(Session::has('done'))
						                <div class="alert bg-success alert-dismissible fade in mb-2" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											{{ Session::get('done') }}
										</div>
						            @endif

									<form action="{{ url($url_admin.'/add-anime-genre-with-anime-id/'.$anime_id) }}" method="POST" class="form">

										{!! csrf_field() !!}

										<div class="form-body">
											<h4 class="form-section"><i class="ft-file-text"></i> Tambah Anime Genre Baru</h4>
											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
														<label>Genre</label>
														<input type="text" class="form-control" placeholder="Genre" name="genre_text" value="{{ old('genre_text') }}">
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label>Genre Slug</label>
														<input type="text" class="form-control" placeholder="Genre Slug" name="genre_slug" value="{{ old('genre_slug') }}">
													</div>
												</div>
											</div>
										</div>

										<div class="form-actions">
											<a href="{{ url($url_admin.'/anime-genre-with-anime-id/'.$anime_id) }}" class="btn btn-warning mr-1"><i class="ft-x"></i> Batal</a>

											<button type="submit" class="btn btn-primary mr-1">
												<i class="fa fa-check-square-o"></i> Simpan
											</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
        </div>
    </div>
</div>

@endsection