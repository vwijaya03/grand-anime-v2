@extends('admin/header')

@section('content')

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-xs-12 mb-2">
                <h3 class="content-header-title mb-0">Source Host</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-xs-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url($url_admin.'/dashboard') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item active">Source Host
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body"><!-- HTML (DOM) sourced data -->
			<section id="server-processing">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title" id="basic-layout-colored-form-control">Direct Url Exploration Movies Anime</h4>
								<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
								<div class="heading-elements">
									<ul class="list-inline mb-0">
										<li><a data-action="collapse"><i class="ft-minus"></i></a></li>
										<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
										<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
										<li><a data-action="close"><i class="ft-x"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="card-body collapse in">
								<div class="card-block">

									<div class="card-text">
										<p>Direct url exploration movies anime without saving the url to the database</p>
									</div>

									<form class="form">
										<div class="form-body">
											<div class="row">
												<div class="col-md-12">
													<div class="form-group">
														<label for="userinput2">Access Url</label>
														<input type="text" id="userinput2" class="form-control border-primary get_access_url_movies_anime" value="{{ url('/admin-access/movies-anime') }}">
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="userinput1">Protocol</label>
														<input type="text" id="userinput1" class="form-control border-primary get_protocol_movies_anime" value="https">
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="userinput2">Url</label>
														<input type="text" id="userinput2" class="form-control border-primary get_url_movies_anime" value="ww4.gogoanime.io">
													</div>
												</div>
												<div class="col-md-4">
													<div class="form-group">
														<label for="userinput2">Additional Url</label>
														<input type="text" id="userinput2" class="form-control border-primary get_additional_url_movies_anime" value="anime-movies.html?page=1">
													</div>
												</div>
												<div class="col-md-4">
													<div class="form-group">
														<label for="userinput2">Ep Start</label>
														<input type="text" id="userinput2" class="form-control border-primary get_ep_start_movies_anime" value="1">
													</div>
												</div>
												<div class="col-md-4">
													<div class="form-group">
														<label for="userinput2">Ep End</label>
														<input type="text" id="userinput2" class="form-control border-primary get_ep_end_movies_anime" value="900">
													</div>
												</div>
											</div>
										</div>

										<div class="form-actions left">
											<a style="color: white !important;" class="btn btn-primary" onclick="direct_url_exploration_movies_anime()"><i class="fa fa-check-square-o"></i> Start Exploration</a>

											<a style="color: white !important;" class="btn btn-danger clear-notifications"><i class="fa ft-trash"></i> Clear Notifications</a>
										</div>
									</form>

								</div>
							</div>
						</div>
					</div>

					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title" id="basic-layout-colored-form-control">Direct Url Exploration Detail Anime</h4>
								<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
								<div class="heading-elements">
									<ul class="list-inline mb-0">
										<li><a data-action="collapse"><i class="ft-minus"></i></a></li>
										<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
										<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
										<li><a data-action="close"><i class="ft-x"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="card-body collapse in">
								<div class="card-block">

									<div class="card-text">
										<p>Direct url exploration detail anime without saving the url to the database</p>
									</div>

									<form class="form">
										<div class="form-body">
											<div class="row">
												<div class="col-md-12">
													<div class="form-group">
														<label for="userinput2">Access Url</label>
														<input type="text" id="userinput2" class="form-control border-primary get_access_url" value="{{ url('/admin-access/detail-anime') }}">
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="userinput1">Protocol</label>
														<input type="text" id="userinput1" class="form-control border-primary get_protocol" value="https">
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="userinput2">Url</label>
														<input type="text" id="userinput2" class="form-control border-primary get_url" value="ww4.gogoanime.io">
													</div>
												</div>
												<div class="col-md-4">
													<div class="form-group">
														<label for="userinput2">Additional Url</label>
														<input type="text" id="userinput2" class="form-control border-primary get_additional_url" value="category/">
													</div>
												</div>
												<div class="col-md-4">
													<div class="form-group">
														<label for="userinput2">Ep Start</label>
														<input type="text" id="userinput2" class="form-control border-primary get_ep_start">
													</div>
												</div>
												<div class="col-md-4">
													<div class="form-group">
														<label for="userinput2">Ep End</label>
														<input type="text" id="userinput2" class="form-control border-primary get_ep_end">
													</div>
												</div>
											</div>
										</div>

										<div class="form-actions left">
											<a style="color: white !important;" class="btn btn-primary" onclick="direct_url_exploration_detail_anime()"><i class="fa fa-check-square-o"></i> Start Exploration</a>

											<a style="color: white !important;" class="btn btn-danger clear-notifications"><i class="fa ft-trash"></i> Clear Notifications</a>
										</div>
									</form>

								</div>
							</div>
						</div>
					</div>

				    <div class="col-xs-12">
				        <div class="card">
				            <div class="card-header">
				                <h4 class="card-title">Source Host</h4>
				                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
			        			<div class="heading-elements">
				                    <ul class="list-inline mb-0">
				                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
				                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				                    </ul>
				                </div>
				            </div>
				            <div class="card-body collapse in">
								<div class="card-block card-dashboard">
									<a href="{{ url($url_admin.'/add-source-host') }}" class="btn btn-success mr-1 mb-1" target="_blank">Tambah Source Host Baru</a>
									<br><br>
									<table class="table table-striped table-bordered dataex-html5-export server-side-source-host">
										<thead>
											<tr>
												<th>Id</th>
												<th>Protocol</th>
												<th>Url</th>
												<th>Additional Url</th>
												<th>Recent Page</th>
												<th>Ep Start</th>
												<th>Ep End</th>
												<th>Tipe</th>
												<th></th>
											</tr>
										</thead>
									</table>
								</div>
				            </div>
				        </div>
				    </div>
				</div>
			</section>
        </div>
    </div>
</div>

@endsection

@section('server_side_datatable')

<script type="text/javascript">
	$(document).ready(function() {
		// $("#sticky").on("click", function() {
	 //        toastr.info("I do not think that word means what you think it means.", "Sticky!", {
	 //            timeOut: 0,
	 //            closeButton: !0
	 //        });

	 //        toastr.clear();
	 //    });

	    $('.server-side-source-host').DataTable({
	    	"lengthMenu": [[10, 25, 50, 100, 200], [10, 25, 50, 100, 200]],
	        "processing": true,
	        "serverSide": true,
	        "ajax":{
	        	"type": "POST",
            	"url": "{{ url($url_admin.'/source-host-ajax') }}",
            	"dataType": "json",
           	},
	        "columns": [
	            { "data": "id" },
	            { "data": "protocol" },
	            { "data": "url" },
	            { "data": "additional_url" },
	            { "data": "recent_page" },
	            { "data": "episode_start_page" },
	            { "data": "episode_end_page" },
	            { "data": "tipe" },
	            { "data": "action_btn" }
	        ]	 

	    });
	});
</script>

@endsection