@extends('admin/header')

@section('content')

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-xs-12 mb-2">
                <h3 class="content-header-title mb-0">Data Anime</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-xs-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url($url_admin.'/dashboard') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item"><a href="{{ url($url_admin.'/anime') }}">Anime</a>
                            </li>
                            <li class="breadcrumb-item active">Tambah Anime Baru
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body"><!-- HTML (DOM) sourced data -->
        	<section id="basic-form-layouts">
				<div class="row match-height">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title" id="basic-layout-form">Anime</h4>
								<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
								<div class="heading-elements">
									<ul class="list-inline mb-0">
										<li><a data-action="collapse"><i class="ft-minus"></i></a></li>
										<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="card-body collapse in">
								<div class="card-block">

									@if ($errors->has('sub_category'))
										<div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<strong>Sub Category</strong> tidak boleh kosong !
										</div>
									@endif

									@if ($errors->has('img'))
										<div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<strong>Image Path</strong> tidak boleh kosong !
										</div>
									@endif

									@if ($errors->has('title'))
										<div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<strong>Title</strong> tidak boleh kosong !
										</div>
									@endif

									@if ($errors->has('description'))
										<div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<strong>Description</strong> tidak boleh kosong !
										</div>
									@endif

									@if ($errors->has('released'))
										<div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<strong>Released</strong> tidak boleh kosong !
										</div>
									@endif

									@if ($errors->has('status'))
										<div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<strong>Status</strong> tidak boleh kosong !
										</div>
									@endif

									@if(Session::has('done'))
						                <div class="alert bg-success alert-dismissible fade in mb-2" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											{{ Session::get('done') }}
										</div>
						            @endif

									<form action="{{ url($url_admin.'/add-anime') }}" method="POST" class="form">

										{!! csrf_field() !!}

										<div class="form-body">
											<h4 class="form-section"><i class="ft-file-text"></i> Tambah Anime Baru</h4>
											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
														<label>Sub Category</label>
														<input type="text" class="form-control" placeholder="Sub Category" name="sub_category" value="{{ old('sub_category') }}">
													</div>
												</div>

												<div class="col-md-6">
													<div class="form-group">
														<label>Title</label>
														<input type="text" class="form-control" placeholder="Title" name="title" value="{{ old('title') }}">
													</div>
												</div>

												<div class="col-md-12">
													<div class="form-group">
														<label>Image Path</label>
														<input type="text" class="form-control" placeholder="Image Path" name="img" value="{{ old('img') }}">
													</div>
												</div>

												<div class="col-md-12">
													<div class="form-group">
														<label>Title</label>
														<textarea class="form-control" id="placeTextarea" name="description" placeholder="Description" rows="5">{{ old('description') }}</textarea>
													</div>
												</div>

												<div class="col-md-4">
													<div class="form-group">
														<label>Released</label>
														<input type="text" class="form-control" placeholder="Released" name="released" value="{{ old('released') }}">
													</div>
												</div>

												<div class="col-md-4">
													<div class="form-group">
														<label>Status</label>
														<input type="text" class="form-control" placeholder="Status" name="status" value="{{ old('status') }}">
													</div>
												</div>
											</div>
										</div>

										<div class="form-actions">
											<a href="{{ url($url_admin.'/anime') }}" class="btn btn-warning mr-1"><i class="ft-x"></i> Batal</a>

											<button type="submit" class="btn btn-primary mr-1">
												<i class="fa fa-check-square-o"></i> Simpan
											</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
        </div>
    </div>
</div>

@endsection