@extends('admin/header')

@section('content')

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-xs-12 mb-2">
                <h3 class="content-header-title mb-0">Data Source Host</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-xs-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url($url_admin.'/dashboard') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item"><a href="{{ url($url_admin.'/source-host') }}">Source Host</a>
                            </li>
                            <li class="breadcrumb-item active">Tambah Source Host Baru
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body"><!-- HTML (DOM) sourced data -->
        	<section id="basic-form-layouts">
				<div class="row match-height">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title" id="basic-layout-form">Tambah Source Host Baru</h4>
								<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
								<div class="heading-elements">
									<ul class="list-inline mb-0">
										<li><a data-action="collapse"><i class="ft-minus"></i></a></li>
										<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="card-body collapse in">
								<div class="card-block">

									@if ($errors->has('protocol'))
										<div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<strong>Protocol</strong> tidak boleh kosong !
										</div>
									@endif

									@if ($errors->has('url'))
										<div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<strong>Url</strong> tidak boleh kosong !
										</div>
									@endif

									@if ($errors->has('tipe'))
										<div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<strong>Tipe</strong> tidak boleh kosong !
										</div>
									@endif

									<form action="{{ url($url_admin.'/add-source-host') }}" method="POST" class="form">

										{!! csrf_field() !!}

										<div class="form-body">
											<h4 class="form-section"><i class="ft-file-text"></i> Tambah Source Host Baru</h4>
											<div class="row">
												<div class="col-md-4">
													<div class="form-group">
														<label>Protocol</label>
														<input type="text" class="form-control" placeholder="Protocol" name="protocol">
													</div>
												</div>

												<div class="col-md-4">
													<div class="form-group">
														<label>Url</label>
														<input type="text" class="form-control" placeholder="Url" name="url">
													</div>
												</div>

												<div class="col-md-4">
													<div class="form-group">
														<label>Additional Url</label>
														<input type="text" class="form-control" placeholder="Additional Url" name="additional_url">
													</div>
												</div>

												<div class="col-md-4">
													<div class="form-group">
														<label>Recent Page</label>
														<input type="text" class="form-control" placeholder="Recent Page" name="recent_page">
													</div>
												</div>

												<div class="col-md-4">
													<div class="form-group">
														<label>Episode Start Page</label>
														<input type="text" class="form-control" placeholder="Episode Start Page" name="episode_start_page">
													</div>
												</div>

												<div class="col-md-4">
													<div class="form-group">
														<label>Episode End Page</label>
														<input type="text" class="form-control" placeholder="Episode End Page" name="episode_end_page">
													</div>
												</div>

												<div class="col-md-4">
													<div class="form-group">
														<label>Tipe</label>
														<input type="text" class="form-control" placeholder="Tipe" name="tipe">
													</div>
												</div>
											</div>
										</div>

										<div class="form-actions">
											<a href="{{ url($url_admin.'/source-host') }}" class="btn btn-warning mr-1"><i class="ft-x"></i> Batal</a>

											<button type="submit" class="btn btn-primary">
												<i class="fa fa-check-square-o"></i> Simpan
											</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
        </div>
    </div>
</div>

@endsection