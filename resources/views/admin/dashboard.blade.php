@extends('admin/header')

@section('content')

<div class="app-content content container-fluid">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-xs-12 mb-2">
        <h3 class="content-header-title mb-0">Dashboard</h3>
        <div class="row breadcrumbs-top">
          <div class="breadcrumb-wrapper col-xs-12">
            <ol class="breadcrumb">
              <li class=""><a href="{{ url('/dashboard') }}">Dashboard</a>
              </li>
            </ol>
          </div>
        </div>
      </div>
    </div>

    <div class="content-body">
        <!-- Description -->
        <div class="card">
          <div class="card-header">
              <h4 class="card-title">Selamat Datang, {{ $user->name }}</h4>
          </div>
        </div>
        <!--/ Description -->
    </div>
  </div>
</div>

@endsection