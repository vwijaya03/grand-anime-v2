<?php
$link_limit = 7; // maximum number of links (a little bit inaccurate, but will be ok for now)
?>

@if ($paginator->lastPage() > 1)
        <div class="pagination2">
            <span>Page {{ $paginator->currentPage() }}:</span>

            @for ($i = 1; $i <= $paginator->lastPage(); $i++)
                <?php
                    $half_total_links = ceil($link_limit / 2);
                    $from = $paginator->currentPage() - $half_total_links;
                    $to = $paginator->currentPage() + $half_total_links; 

                    // Log::info("From ".$from);
                    // Log::info("To ".$to);

                    if ($paginator->currentPage() < $half_total_links) {
                        $to += $half_total_links - $paginator->currentPage();
                    }

                    if ($paginator->lastPage() - $paginator->currentPage() < $half_total_links) {
                        $from -= $half_total_links - ($paginator->lastPage() - $paginator->currentPage()) - 1;
                    }
                ?>
                @if($link_limit % 2 == 0)
                    @if($to > $paginator->lastPage())
                        @if ($from-1 < $i && $i < $to+1)
                            <a class="{{ ($paginator->currentPage() == $i) ? ' active' : '' }}" href="{{ $paginator->url($i) }}">{{ $i }}</a>
                        @endif
                    @else
                        @if ($from < $i && $i < $to+1)
                            <a class="{{ ($paginator->currentPage() == $i) ? ' active' : '' }}" href="{{ $paginator->url($i) }}">{{ $i }}</a>
                        @endif
                    @endif
                @else
                    @if ($from < $i && $i < $to)
                        <a class="{{ ($paginator->currentPage() == $i) ? ' active' : '' }}" href="{{ $paginator->url($i) }}">{{ $i }}</a>
                    @endif
                @endif
                
            @endfor
            
            @if($paginator->currentPage() == $paginator->lastPage() || $paginator->currentPage() > $paginator->lastPage())
            
            @else
                <a href="{{ $paginator->url($paginator->currentPage()+1) }}"><i class="ion-arrow-right-b"></i></a>
            @endif
        </div>
@endif