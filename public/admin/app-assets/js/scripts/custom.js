$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(".clear-notifications").on("click", function() {
    toastr.clear();
});

function slugify(string)
{
    return string
    .toString()
    .trim()
    .toLowerCase()
    .replace(/\s+/g, "-")
    .replace(/[^\w\-]+/g, "")
    .replace(/\-\-+/g, "-")
    .replace(/^-+/, "")
    .replace(/-+$/, "");
}

function direct_url_exploration_movies_anime()
{
    var day = new Date();
    var args = {};
    var get_access_url = '';

    toastr.info("Exploration begin at "+day.getHours()+":"+day.getMinutes()+":"+day.getSeconds(), "Starting", {
        timeOut: 0,
        extendedTimeOut: 0,
        closeButton: !0
    });

    get_access_url = $('.get_access_url_movies_anime').val();
    args.target_url = $('.get_protocol_movies_anime').val()+'://'+$('.get_url_movies_anime').val()+'/'+$('.get_additional_url_movies_anime').val();
    args.base_url = $('.get_protocol_movies_anime').val()+'://'+$('.get_url_movies_anime').val();
    args.ep_start = $('.get_ep_start_movies_anime').val();
    args.ep_end = $('.get_ep_end_movies_anime').val();
    
    $.ajax({
        type: "POST",
        url: get_access_url,
        timeout: 7200000,
        dataType: "json",
        data: args,
        cache : false,
        success: function(data){
            if(data.status == '200')
            {
                day = new Date();
                toastr.success("Exploration done at "+day.getHours()+":"+day.getMinutes()+":"+day.getSeconds(), "Success", {
                    timeOut: 0,
                    extendedTimeOut: 0,
                    closeButton: !0
                });
            }
        } ,error: function(xhr, status, error) {
            console.log(error);
            console.log(status);
            day = new Date();
            toastr.error("Exploration ended at "+day.getHours()+":"+day.getMinutes()+":"+day.getSeconds(), "Error", {
                timeOut: 0,
                extendedTimeOut: 0,
                closeButton: !0
            });
        },
    });
}

function direct_url_exploration_detail_anime()
{
    var day = new Date();
    var args = {};
    var get_access_url = '';

    toastr.info("Exploration begin at "+day.getHours()+":"+day.getMinutes()+":"+day.getSeconds(), "Starting", {
        timeOut: 0,
        extendedTimeOut: 0,
        closeButton: !0
    });

    get_access_url = $('.get_access_url').val();
    args.target_url = $('.get_protocol').val()+'://'+$('.get_url').val()+'/'+$('.get_additional_url').val();
    args.base_url = $('.get_protocol').val()+'://'+$('.get_url').val();
    args.ep_start = $('.get_ep_start').val();
    args.ep_end = $('.get_ep_end').val();
    
    $.ajax({
        type: "POST",
        url: get_access_url,
        timeout: 7200000,
        dataType: "json",
        data: args,
        cache : false,
        success: function(data){
            if(data.status == '200')
            {
                day = new Date();
                toastr.success("Exploration done at "+day.getHours()+":"+day.getMinutes()+":"+day.getSeconds(), "Success", {
                    timeOut: 0,
                    extendedTimeOut: 0,
                    closeButton: !0
                });
            }
        } ,error: function(xhr, status, error) {
            console.log(error);
            console.log(status);
            day = new Date();
            toastr.error("Exploration ended at "+day.getHours()+":"+day.getMinutes()+":"+day.getSeconds(), "Error", {
                timeOut: 0,
                extendedTimeOut: 0,
                closeButton: !0
            });
        },
    });
}

function run_garuk(access_url, protocol, url, additional_url, recent_page, ep_start, ep_end, tipe)
{
    var day = new Date();
    var get_access_url = '';

    toastr.info("Exploration begin at "+day.getHours()+":"+day.getMinutes()+":"+day.getSeconds(), "Starting", {
        timeOut: 0,
        extendedTimeOut: 0,
        closeButton: !0
    });

    var args = {};

    if(tipe == 'recent-release-anime')
    {
        get_access_url = access_url;
        args.target_url = protocol+'://'+url+'/'+additional_url+recent_page;
        args.base_url = protocol+'://'+url;
    }
    else if(tipe == 'detail-anime')
    {
        get_access_url = access_url;
        args.target_url = protocol+'://'+url+'/'+additional_url;
        args.base_url = protocol+'://'+url;
        args.ep_start = ep_start;
        args.ep_end = ep_end;
    }

    $.ajax({
        type: "POST",
        url: get_access_url,
        timeout: 7200000,
        dataType: "json",
        data: args,
        cache : false,
        success: function(data){
            if(data.status == '200')
            {
                day = new Date();
                toastr.success("Exploration done at "+day.getHours()+":"+day.getMinutes()+":"+day.getSeconds(), "Success", {
                    timeOut: 0,
                    extendedTimeOut: 0,
                    closeButton: !0
                });
            }
        } ,error: function(xhr, status, error) {
            console.log(error);
            console.log(status);
            day = new Date();
            toastr.error("Exploration ended at "+day.getHours()+":"+day.getMinutes()+":"+day.getSeconds(), "Error", {
                timeOut: 0,
                extendedTimeOut: 0,
                closeButton: !0
            });
        },
    });

    /* Debug Error*/

    // $.ajax({
    //     type: "POST",
    //     url: get_access_url,
    //     dataType: "json",
    //     data: args,
    //     cache : false
    // }).fail(function($xhr) {
    //     var data = $xhr.responseJSON;
    //     console.log($xhr);
    // });
}

function master_edit(url)
{
    var execute_url = url;
    console.log(execute_url);
    window.location.href = execute_url;
}

function master_delete(url, master_data)
{
    swal({
        title: "Apakah anda yakin ?",
        text: "Data tidak bisa di kembalikan !",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yakin !',
        cancelButtonText: "Batal !",
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function(isConfirm) {
        if (isConfirm) {

            var execute_url = url;

            $.ajax({
                type: "POST",
                url: execute_url,
                dataType: "json",
                cache : false,
                success: function(data){
                    swal({
                        title: 'Berhasil Di Hapus',
                        text: 'Data berhasil di hapus!',
                        type: 'success'
                    }, function() {
                        window.location.href = master_data;
                    });
                } ,error: function(xhr, status, error) {
                  console.log(error);
                },

            });
        } else {
            swal("Cancelled", "", "error");
        }
    });
}